@extends('page')


@section('custom-css')
<style>
    body{
        background-image: url(" {{asset('images/fondo1.jpg')}} ");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #titulo_banquetes{
        font-family: 'Oleo Script', cursive;
    }

    form{
        text-align: center;
        justify-content: center;
    }
</style>
@endsection


@section('contenido')

    <!-- Row -->
    <div class="container" Style="width: 500px;">
        @include('sweetalert::alert')

            {{-- COLUMNA DERECHA --}}
            <div  style="background: #000000;
                        border-radius: 10px;
                        align-content: center;">
            <h2 class="fw-bold text-left py-5" Style="margin-left: 150px; color: #fff;" >REGISTRARSE</h2>

            {{-- FORMULARIO --}}
                <form method="POST" action="{{ route('register') }}">
                        @csrf

                    {{-- NOMBRE --}}
                    <div class="form-group row" Style="color: #fff;">
                        <label for="name" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right"><strong>{{ __('Username') }}*</strong></label>

                        <div class="col-md-6">
                        <input id="namemy" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Ingresa un nombre">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                        
                    {{-- EMAIL --}}

                    <div class="form-group row" Style="color: #fff;">
                        <label for="email" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right"><strong>{{ __('E-Mail Address') }}*</strong></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ingresa un correo electronico">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- CONTRASEÑA --}}
                    <div class="form-group row" Style="color: #fff;">
                        <label for="password" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right"><strong>{{ __('Password') }}*</strong></label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Ingresa una contraseña">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{-- CONFIRMAR CONTRASEÑA --}}
                    <div class="form-group row" Style="color: #fff;">
                            <label for="password-confirm" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right"><strong>{{ __('Confirm Password') }}*</strong></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirma la contraseña">
                            </div>
                    </div>

                    {{-- BOTON REGISTRARSE --}}
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" style="margin-left: 125px" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                    </div><br><br>
                </form>
                {{-- FIN FORMULARIO --}}
            </div>

    </div>

<!-- End Row-->
@endsection

