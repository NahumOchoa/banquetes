@extends('page')


@section('custom-css')
<style>
    body{
        background-image: url(" {{asset('images/background_login3.png')}} ");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #titulo_banquetes{
        font-family: 'Oleo Script', cursive;
    }

    form{
        text-align: center;
        justify-content: center;
    }
</style>
@endsection



@section('contenido')

    <!-- Row -->
    <div class="container" Style="width: 500px;">

            {{-- COLUMNA DERECHA --}}
            <div  style="background: #777259;
                        border-radius: 10px;
                        align-content: center;">
            <h2 class="fw-bold text-left py-5" Style="margin-left: 150px; color: #fff;" >{{ __('Verify Your Email Address') }}</h2>

                <div class="card-body" Style="color: #fff;">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.send') }}">
                        @csrf
                        <br><br><button type="submit" class="btn btn-primary">{{ __('click here to request another') }}</button><br><br>
                    </form>
                </div>
            </div>

    </div>

<!-- End Row-->
@endsection


