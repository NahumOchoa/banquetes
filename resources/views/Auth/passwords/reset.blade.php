@extends('page')


@section('custom-css')
<style>
    body{
        background-image: url(" {{asset('images/background_login3.png')}} ");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #titulo_banquetes{
        font-family: 'Oleo Script', cursive;
    }

    form{
        text-align: center;
        justify-content: center;
    }
</style>
@endsection


@section('contenido')

    <!-- Row -->
    <div class="container" Style="width: 500px;">

            {{-- COLUMNA DERECHA --}}
            <div  style="background: #777259;
                        border-radius: 10px;
                        align-content: center;">
            <h2 class="fw-bold text-left py-5" Style="margin-left: 150px; color: #fff;" >{{ __('Reset Password') }}</h2>

                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $request->token }}">
                    <div class="form-group row" style="color: #fff">
                            <label for="email" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ $request->email }}" required autocomplete="email" autofocus >
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                    </div>
                    <div class="form-group row" Style="color: #fff;">
                        <label for="password" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right">
                            <strong>{{ __('Password') }}*</strong>
                        </label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="new-password" placeholder="Ingresa una contraseña">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row" Style="color: #fff;">
                            <label for="password-confirm" style="margin-bottom: 20px" class="col-md-4 col-form-label text-md-right">
                                <strong>{{ __('Confirm Password') }}*</strong>
                            </label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                       required autocomplete="new-password" placeholder="Confirma la contraseña">
                            </div>
                    </div>

                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" style="margin-left: 125px" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                    </div><br><br>
                </form>
            </div>

    </div>

<!-- End Row-->
@endsection
