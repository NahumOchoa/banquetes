@extends('auth-template')

@extends('page')


@section('custom-css')
<style>
    body{
        background-image: url(" {{asset('images/background_login3.png')}} ");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #titulo_banquetes{
        font-family: 'Oleo Script', cursive;
    }

    form{
        text-align: center;
        justify-content: center;
    }
</style>
@endsection


@section('contenido')

    <!-- Row -->
    <div class="container" Style="width: 500px;">

            {{-- COLUMNA DERECHA --}}
            <div  style="background: #777259;
                        border-radius: 10px;
                        align-content: center;">
            <h2 class="fw-bold text-left py-5" Style="margin-left: 150px; color: #fff;" >{{ __('Reset Password') }}</h2>

            <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row" Style="color: #fff;">
                            <label for="email" class="col-md-4 col-form-label text-md-right" Style="margin-left: 15px"><strong>{{ __('E-Mail Address') }}</strong></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ingresa tu correo electronico" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 " Style="margin-left: 60px">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div><br><br>
                    </form>
            </div>

    </div>

<!-- End Row-->
@endsection









