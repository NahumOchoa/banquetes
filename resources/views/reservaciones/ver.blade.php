@extends('app')

@section('extra-css')
<style>
	#estimadoTotal{
		border:none;
		font-weight: bold;
		font-size: 22px;
		text-align: right;
		color: #7871df;
	}

	#mytable{
		border-collapse: collapse;
		border-radius: 15px;
		-webkit-box-shadow: 2px 2px 5px rgb(230, 228, 228);
 		-moz-box-shadow: 2px 2px 5px rgb(230, 228, 228);
	}

	#mytable thead th{
		color: #fff;
		background-color: #6c7ae0;
	}
	#mytable thead th:first-child{
		border-top-left-radius: 15px;
	}
	#mytable thead th:last-child{
		border-top-right-radius: 15px;
	}

	.filaSeparador{
		background-color: #f8f6ff;
	}
	.separador{
		border-top: 1px solid grey;
		font-weight: 500;
	}

	.img{
		width:80px;
		height:60px;
		/* width: 18%; */
		border-radius: 5px;
		margin-right: 2%;
	}
</style>

@endsection

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver reservación #{{$booking->id}}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="/reservaciones">Reservaciones</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver reservación #{{$booking->id}}</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

	<!-- Fila detalles-->
	<div class="row">

		{{-- Tarjeta información --}}
	    <div class="col-xl-3 col-md-12 col-lg-12">
	        <div class="card">
				<div class="card-header d-xl-flex d-block">
					<div class="card-leftheader">
						<h4 class="card-title" id="impuestos">Detalles de la reservación</h4>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Nombre del evento:</font>
						<span class="mb-2 fs-14">{{$booking->name}}</span>
					</div>
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Descripción:</font>
						<span class="mb-2 fs-14">{{$booking->description}}</span>
					</div>
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Cliente:</font>
						<span class="mb-2 fs-14">{{$booking->user->name}}</span>
					</div>
					<div class="row">
						<div class="col-6">
							<font style="vertical-align: inherit; font-weight:bold">Fecha:</font> <br>
							<span class="mb-2 fs-14">{{$booking->booking_date}}</span>
						</div>
						<div class="col-6">
							<font style="vertical-align: inherit; font-weight:bold">Hora:</font> <br>
							<span class="mb-2 fs-14">{{$booking->booking_time}}</span>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-6">
							{{-- <button type="button" class="btn btn-primary">Contrato</button> --}}
							<a href="{{asset('/contracts' . '/CONTRACT-' . strval($booking->id).strval($booking->user->id) . '.pdf')}}" class="btn btn-primary" target="_blank">Contrato</a>
						</div>
						<div class="col-6">
							<a href="{{asset('/invoices' . '/FACT-' . strval($booking->id).strval($booking->user->id) . '.pdf')}}" class="btn btn-success" target="_blank">Factura</a>
							{{-- <button type="button" class="btn btn-success">Facturas</button> --}}
						</div>
					</div>
				</div>
	        </div>
	    </div>

		{{-- Tarjeta platillos --}}
		<div class="col-xl-9 col-md-12 col-lg-12">
			<div class="card">
				<div class="card-header d-xl-flex d-block">
					<div class="card-leftheader">
						<h4 class="card-title">Menú de la reservación</h4>
					</div>
				</div>

				<div class="card-body">
					{{-- Tabla platillos --}}
					<div class="row">
						<div class="table-responsive">
							<table class="table  text-wrap border-bottom table-borderless" id="mytable">
								<thead>
									<tr>
										<th class="border-bottom-0 text-center" width="60%">Nombre</th>
										<th class="border-bottom-0 text-center" width="15%" align="right">Cantidad</th>
										<th class="border-bottom-0 text-center" width="15%" align="right">Monto</th>
									</tr>
								</thead>
								<tbody id="lista_platillos">
									<tr class="filaSeparador">
										<td colspan="5" class="separador">Platillos</td>
									</tr>
		                            <tr>
		                                <td>
											<div class="d-flex">
												@if (isset($booking->booking->menu->url_img))
													<img class='img' src='{{asset($booking->booking->menu->url_img)}}'>
                        						@else
													<img class='img' src='{{asset($booking->booking->package->menu->url_img)}}'>
                        						@endif
												
												{{-- <span class="avatar-xl bradius mr-4" style="background-image: url('{{asset($booking->booking->menu->url_img)}}'"></span> --}}
												<div class="mr-3 mt-0 mt-sm-1 d-block">
													@if (isset($booking->booking->menu))
														<h6 class="mb-1 fs-14">{{$booking->booking->menu->name}}</h6>
														<p class="text-muted mb-0 fs-12">{{$booking->booking->menu->description}}</p>
                        							@else
														<h6 class="mb-1 fs-14">{{$booking->booking->package->description}}</h6>
                        							@endif
												</div>
											</div>
										</td>
		                                <td align="right">
		                                    <span>{{$booking->quantity}}</span>
		                                </td>
		                                <td align="center">
		                                    <span id="monto"></span>
		                                </td>
		                            </tr>
									@isset($services)
										<tr class="filaSeparador">
											<td colspan="5" id="separadorServicios" class="separador">Servicios</td>
										</tr>
										@foreach ($services as $service)
										<tr>
											<td>
												<div class="d-flex">
													{{-- <span class="avatar-xl bradius mr-4" style="background-image: url('{{asset($complement->url_image)}}'"></span> --}}
													<img class='img' src='{{asset($service->url_img)}}'>
													<div class="mr-3 mt-0 mt-sm-1 d-block">
														<h6 class="mb-1 fs-14">{{$service->name}}</h6>
														<p class="text-muted mb-0 fs-12">{{$service->description}}</p>
													</div>
												</div>
											</td>
											<td align="right">
												<span>{{$service->pivot->quantity}}</span>
											</td>
											<td align="center">
												<span class="montoServicio"></span>
											</td>
										</tr>
										@endforeach
									@endisset
									@isset($complements)
										<tr class="filaSeparador">
											<td colspan="5" id="separadorComplemento" class="separador">Complementos</td>
										</tr>
										@foreach ($complements as $complement)
										@isset($complement)
										<tr>
											<td>
												<div class="d-flex">
													{{-- <span class="avatar-xl bradius mr-4" style="background-image: url('{{asset($complement->url_image)}}'"></span> --}}
													<img class='img' src='{{asset($complement->url_image)}}'>
													<div class="mr-3 mt-0 mt-sm-1 d-block">
														<h6 class="mb-1 fs-14">{{$complement->name}}</h6>
														<p class="text-muted mb-0 fs-12">Sin descripción</p>
													</div>
												</div>
											</td>
											<td align="right">
												<span>1</span>
											</td>
											<td align="center" class="text-success font-weight-bold">¡Gratis!</td>
										</tr>
										@endisset
										@endforeach
									@endisset
		                        </tbody>
								<tfoot>
									<tr>
										<td></td>
										<td align="right" width="15%"><h6 class="mb-1 fs-17 text-muted">Total:</h6></td>
										<td width="25%" align="center"><input type="text" class="form-control mb-md-1 mb-5 fs-17 text-center" id="estimadoTotal" name="total" readonly></td>
									</tr>
								</tfoot>
		                    </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Row-->

	<!-- Fila ubicacion y contacto-->
	<div class="row">
		<div class="col-xl-12 col-md-12 col-lg-12">
	        <div class="card">
				<div class="card-header d-xl-flex d-block">
					<div class="card-leftheader">
						<h4 class="card-title" id="impuestos">Ubicación del evento</h4>
					</div>
				</div>
				<div class="card-body">
					<div class="row">

					</div>
					<div class="row">
						<div class="col-5">
							<div class="d-flex">
								<span class="avatar-xl mr-1">
									<i class="feather feather-map-pin text-primary"></i>
								</span>
								<div class="mr-3 mt-0 mt-sm-1 d-block">
									<font style="vertical-align: inherit; font-weight:bold">Dirección:</font> <br>
									<span class="mb-2 fs-14">{{$booking->address->street_name}}. México, {{$booking->address->zip_code}} Cd Victoria, Tamaulipas. <br> {{$booking->address->description}}</span>
								</div>
							</div>
						</div>
						<div class="col-4">
							<div class="d-flex">
								<span class="avatar avatar-md brround mr-3" style="background-image: url({{asset('images/usuario.png')}}"></span>
								<div class="mr-3 mt-0 mt-sm-1 d-block">
									<font style="vertical-align: inherit; font-weight:bold">Cliente:</font> <br>
									<span class="mb-2 fs-14">{{$booking->user->name}}</span>
								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="d-flex">
								<span class="avatar-xl" style="margin-right: -15px;">
									<i class="feather feather-mail text-primary"></i>
								</span>
								<div class="mr-3 mt-0 mt-sm-1 d-block">
									<font style="vertical-align: inherit; font-weight:bold">Correo electrónico:</font> <br>
									<span class="mb-2 fs-14">{{$booking->user->email}}</span>
								</div>
							</div>
							{{-- <font style="vertical-align: inherit; font-weight:bold">Telefono:</font> <br>
							<span class="mb-2 fs-14">834 171 1100</span> --}}
						</div>
					</div>
				</div>
                <div class="card-footer text-right">
                        <a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					        <i class="feather feather-corner-down-left sidemenu_icon"></i>
					        Regresar
				        </a>
                    </div>
	        </div>
	    </div>
	</div>
@endsection



@section('extra-script')
<script>
	$(function() {
		window.escribirTotal = function(){
			let monto = document.getElementById('monto');
			let total = document.getElementById('estimadoTotal');

			monto.innerText = numberToMoney({{$booking->subtotal}});
			total.setAttribute('value', numberToMoney({{$booking->total}}));
		}
		escribirTotal();
	});

	$(function() {
		window.escribirMontoServicios = function(){
			let casillasMonto = document.getElementsByClassName('montoServicio');
			let servicios = @json($services);

			for (let i = 0; i < casillasMonto.length; i++) {
				casillasMonto[i].innerText = numberToMoney(servicios[i]['pivot']['quantity'] * servicios[i]['price']);
			}
		}
		escribirMontoServicios();
	});

	/*
		Da formato de moneda a String
	*/
	function numberToMoney(value) {
		const formatterDolar = new Intl.NumberFormat('en-US', {
    		style: 'currency',
       		currency: 'USD'
     	});
		if (isNaN(value)) {
			value = 0;
		}

		return formatterDolar.format(value);
	}
</script>

@endsection
