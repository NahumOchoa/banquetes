@extends('app')

@section('extra-css')
<style>
	.cantidad, #estimadoTotal{
		text-align: right;
	}

	#estimadoTotal{
		border:none;
		font-weight: bold;
		font-size: 22px;
		color: #7871df;
	}

	.deshabilita{
	  pointer-events:none !important;
	}

	#mytable{
		border-collapse: collapse;
		border-radius: 15px;
		-webkit-box-shadow: 2px 2px 5px rgb(230, 228, 228);
 		-moz-box-shadow: 2px 2px 5px rgb(230, 228, 228);
	}

	#mytable thead th{
		color: #fff;
		background-color: #6c7ae0;
	}
	#mytable thead th:first-child{
		border-top-left-radius: 15px;
	}
	#mytable thead th:last-child{
		border-top-right-radius: 15px;
	}

	.filaSeparador{
		background-color: #f8f6ff;
	}
	.separador{
		border-top: 1px solid grey;
		font-weight: 500;
	}

	/* Estilos para contenido de Select de platillos */
	.opciones{
		display: flex;
		margin-top: 1.5%;
		width: 100%;
	}
	.opcionImg{
		width:100px;
		height:80px;
		border-radius: 5px;
		margin-right: 2%;
	}
	.opcionInfo{
		width: 500px;
	}
	.opcionTxt{
		font-weight:bold;
		font-size:12pt;
	}
	.descrpition{
		margin-top: -25px;
	}


	/* Modificar tamaño de los Select */
	.carclass .select2-selection__rendered {
            line-height: 100px !important;
    }

   	.carclass.select2-container .select2-selection--single {
            height: 100px !important;
    }

    .carclass .select2-selection__arrow {
            height: 100px !important;
    }
</style>

@endsection

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Editar reservación #{{$booking->id}}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="/reservaciones">Reservaciones</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Editar reservación</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12 col-md-12 col-lg-12">
			<div class="card">
				<div class="card-body">
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<form action="{{ route('reservaciones.update', $booking->id) }}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						@csrf

						<h4 class="mb-5 font-weight-semibold">Detalles de la reservación</h4>

						{{-- Primera fila del formulario --}}
						<div class="row">
							{{-- Nombre del evento --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Nombre del evento:</label>
									<input class="form-control" placeholder="Ingresa nombre del evento" name="name" value="{{$booking->name}}" type="text" maxlength="50">
								</div>
							</div>
							{{-- Fecha del evento --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Fecha del evento:</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="feather feather-calendar"></i>
											</div>
										</div>
										<input class="form-control fc-datepicker" placeholder="YYYY-MM-DD" type="date" name="booking_date" value="{{$booking->booking_date}}">
									</div>
								</div>
							</div>
							{{-- Hora del evento --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Hora del evento:</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="feather feather-clock"></i>
											</div>
										</div><input class="form-control fc-datepicker" placeholder="YYYY-MM-DD" type="time" name="booking_time" value="{{$booking->booking_time}}">
									</div>
								</div>
							</div>
							{{-- Cliente --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Cliente:</label>
									<select class="form-control custom-select select2" data-placeholder="Selecciona un cliente" id="select_cliente" name="user_id">
										<option value="{{$booking->user->id}}" selected>{{$booking->user->name}}</option>
										@foreach($clientes as $cliente)
											<option value="{{$cliente->id}}">{{$cliente->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						{{-- Segunda fila del formulario --}}
						<div class="row">
							{{-- Descripción --}}
							<div class="col-md-4">
								<div class="form-group">
									<label class="form-label">Descripción:</label>
									<textarea rows="3" class="form-control" name="description" placeholder="description" maxlength="250">{{$booking->description}}</textarea>
								</div>
							</div>
						</div>

						<br><br>

						<h4 class="mb-5 font-weight-semibold">Dirección del evento</h4>

						{{-- Primera fila del formulario --}}
						<div class="row">
							{{-- Estado --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Estado:</label>
									<input class="form-control" disabled name="state" value="Tamaulipas">
								</div>
							</div>
							{{-- Ciudad --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Ciudad:</label>
									<input class="form-control" disabled name="city" value="Ciudad Victoria">
								</div>
							</div>
						</div>

						<div class="row">
							{{-- Código postal --}}
							<div class="col-md-3">
								<div class="form-group">
									<label class="form-label">Código postal:</label>
									<input class="form-control" name="zip_code" maxlength="5" type="number" value="{{$booking->address->zip_code}}" min="0">
								</div>
							</div>
							{{-- Calle --}}
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label">Calle:</label>
									<input class="form-control" placeholder="Escribe solo el nombre de la calle o avenida" name="street_name" value="{{$booking->address->street_name}}" type="text" maxlength="200">
								</div>
							</div>
						</div>
						<div class="row">
							{{-- Descripción --}}
							<div class="col-md-4">
								<div class="form-group">
									<label class="form-label">Descripción de la dirección:</label>
									<textarea rows="3" class="form-control" name="description_address" placeholder="Ej. A un costado de GranD Campestre" maxlength="250">{{$booking->address->description}}</textarea>
								</div>
							</div>


						</div>

						<br><br>

						<div class="d-flex">
							<h4 class="mb-5 font-weight-semibold">Arma el menú perfecto para el evento </h4>
							<label> (Recuerda que puedes agregar hasta dos complementos)</label>
						</div>



						{{-- Tabla de platillos --}}
						<div class="row">
							<div class="table-responsive">
								<table class="table  text-wrap border-bottom table-borderless" id="mytable">
									<thead>
										<tr>
											<th class="border-bottom-0 text-center" width="70%">Nombre</th>
											<th class="border-bottom-0 text-center" width="10%" align="right">Cantidad</th>
											<th class="border-bottom-0 text-center" width="10%" align="right">Precio unitario</th>
											<th class="border-bottom-0 text-center" width="10%" align="right">Monto</th>
											<th class="border-bottom-0 text-center"> </th>
										</tr>
									</thead>
									<tbody id="lista_platillos">
										<tr class="filaSeparador">
											<td colspan="5" class="separador">Platillos</td>
										</tr>
										<tr onchange="obtenerCostoPlatillo();">
											<td width="70%" style="height: 150px;">
												<select name="platillo" class="form-control platillos" onchange="obtenerCostoPlatillo();" required style="width: 100%">
													@foreach ($categoriasMenu as $categoria)
													<option value="-1" class="text-muted" selected disabled>{{$categoria->name}}</option>
														@foreach ($platillos as $platillo)
															 @if($platillo->category_id == $categoria->id)
															 	<option data-img_src="{{asset($platillo->url_img)}}" value="{{$platillo->id}}" class="platillosReservacion">{{$platillo->name}} $ {{$platillo->description}}</option>
															 @endif
														@endforeach
													@endforeach
													@if (isset($booking->booking->menu))
														<option data-img_src="{{asset($booking->booking->menu->url_img)}}" value="{{$booking->booking->menu->id}}" class="platillosReservacion" selected>{{$booking->booking->menu->name}} $ {{$booking->booking->menu->description}}</option>
                        							@else
														<option data-type="paquete" data-img_src="{{asset($booking->booking->package->menu->url_img)}}" value="{{$booking->booking->package->id}}" class="platillosReservacion" selected>{{$booking->booking->package->description}} $ </option>
														{{-- <h6 class="mb-1 fs-14">{{$booking->booking->package->description}}</h6> --}}
                        							@endif
													
												</select>
											</td>

											<td width="10%"><input value="{{$booking->quantity}}" type="number" class="form-control mb-md-1 mb-5 cantidad" value=1 min="1" onchange="obtenerCostoPlatillo();" onkeyup="obtenerCostoPlatillo();" align="right" name="quantity"></td>
											<td width="10%" class="precio" align="right">$0.00</td>
											<td width="10%" class="monto" align="right">$0.00</td>
											<td width="10%">
												{{-- <a class="action-btns1" title="Remover" onclick="eliminarFila()" hidden><i class="feather feather-x text-danger"></i></a> --}}
											</td>
										</tr>
										@isset($complementosEnReservacion)
											<tr class="filaSeparador">
												<td colspan="5" id="separadorComplemento" class="separador">Complementos</td>
											</tr>
											@foreach ($complementosEnReservacion as $complementoSeleccionado)
											@isset($complementoSeleccionado)
											<tr>
												<td width="70%" style="height: 150px;">
													<select class="complementos" style="width: 100%;" name="complementos[]">
														@foreach ($complementos as $complemento)
															<option data-img_src="{{asset($complemento->url_image)}}" value="{{$complemento->id}}" class="platillosReservacion">{{$complemento->name}} $ Sin descripción</option>
														@endforeach
														<option data-img_src="{{asset($complementoSeleccionado->url_image)}}" value="{{$complementoSeleccionado->id}}" class="platillosReservacion" selected>{{$complementoSeleccionado->name}} $ Sin descripción</option>
													</select>
												</td>
												<td width="10%"><input disabled type="number" class="form-control mb-md-1 mb-5 text-right" value=1 min="1"></td>
												<td width="10%" align="right" class="text-success font-weight-bold">¡Gratis!</td>
												<td width="10%" align="right" class="text-success font-weight-bold">¡Gratis!</td>
											</tr>
											@endisset
											@endforeach
										@endisset
										@isset($serviciosActuales)
											<tr class="filaSeparador">
												<td colspan="5" id="separadorServicios" class="separador">Servicios</td>
											</tr>
											@foreach ($serviciosActuales as $servicioSeleccionado)
											<tr>
												<td width="70%" style="height: 150px;"> 
													<select onchange="obtenerCostoServicio(event);" class="servicios" style="width: 100%;" name="servicios[]">
														<option selected data-img_src="{{asset($servicioSeleccionado->url_img)}}" value="{{$servicioSeleccionado->id}}" class="platillosReservacion optionComplemento">{{$servicioSeleccionado->name}} $ {{$servicioSeleccionado->description}}</option> 
														@foreach ($servicios as $servicio) 
															<option data-img_src="{{asset($servicio->url_img)}}" value="{{$servicio->id}}" class="platillosReservacion optionComplemento">{{$servicio->name}} $ {{$servicio->description}}</option> 
														@endforeach 
													</select> 
												</td>
												<td width="10%">
													<input type="number" class="form-control mb-md-1 mb-5 cantidad" value={{$servicioSeleccionado->pivot->quantity}} min="1" onchange="obtenerCostoServicio(event);" onkeyup="obtenerCostoServicio(event);" align="right" name="quantityServices[]">
												</td>
												<td width="10%" class="precioServicio" align="right">{{$servicioSeleccionado->price}}</td>
												<td width="10%" class="montoServicio" align="right">$0.00</td>
												<td width="10%"><a class="action-btns1" title="Remover" onclick="eliminarServicio()"><i class="feather feather-x text-danger"></i></a></td>
											</tr>
											@endforeach
										@endisset
									</tbody>
								</table>

								<button type="button" class="btn btn-outline-info mr-2" onclick="agregarComplemento()" id="btnAgregaComplemento">Agregar Complemento</button>
								<button type="button" class="btn btn-outline-info mr-2" onclick="agregarServicio()" id="btnAgregaComplemento">Agregar Servicio</button>

								<table class="table text-nowrap" id="hr-table">
									<tbody>
										<tr class="border-bottom">
											<td></td>
											<td align="right" width="15%"><h6 class="mb-1 fs-17 text-muted">Total:</h6></td>
											<td width="15%"><input class="form-control mb-md-1 mb-5 fs-17" id="estimadoTotal" name="total" value="$0.00" readonly></td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>
					</div>
					<div class="card-footer text-right">
                        <a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					        <i class="feather feather-corner-down-left sidemenu_icon"></i>
					        Regresar
				        </a>
						<button type="submit" class="btn btn-primary" id="enviar">
							<i class="feather feather-save sidemenu_icon"></i>
							Guardar
						</button>
					</div>
				</form>
			</div>
		</div>
		<!-- End Row-->
@endsection




@section('extra-script')
<script src="{{ asset('js/select2Template.js') }}"></script>
<script src="{{ asset('js/number2Money.js') }}"></script>
<script src="{{ asset('js/confirmExit.js') }}"></script>
<script src="{{ asset('js/characterCounter.js') }}"></script>
<script src="{{ asset('js/bookingUtilities/complementos.js') }}"></script>
<script src="{{ asset('js/bookingUtilities/platillos.js') }}"></script>
<script src="{{ asset('js/bookingUtilities/servicios.js') }}"></script>

<script>
	$(function() {
		window.escribirTotal = function(){
			obtenerCostoPlatillo();

			let option = document.querySelector('.platillos').querySelector('option:checked');
			let select = document.querySelector('.platillos');
					
			if (option.getAttribute('data-type') == 'paquete') {
				select.setAttribute('name', 'paquete');
			}


			var options = {
				'templateSelection': custom_template,
				'templateResult': custom_template,
			}


			$('.complementos').select2(options);

			let selectsPlatillos = document.getElementsByClassName('complementos');

			for (let index = 0; index < $('.complementos').length; index++) {
				selectsPlatillos[index].nextSibling.classList.add("carclass");
			}


			$('.servicios').select2(options);

			let selectsServicios = document.getElementsByClassName('servicios');

			for (let index = 0; index < $('.servicios').length; index++) {
				selectsServicios[index].nextSibling.classList.add("carclass");
			}
		}
		escribirTotal();
	});

	$(function() {
		window.escribirMontoServicios = function(){
			let casillasMonto = document.getElementsByClassName('montoServicio');
			let serviciosPrecio = document.getElementsByClassName('precioServicio');
			let servicios = @json($serviciosActuales);

			for (let i = 0; i < casillasMonto.length; i++) {
				serviciosPrecio[i].innerText = numberToMoney(servicios[i]['price']);
				casillasMonto[i].innerText = numberToMoney(servicios[i]['pivot']['quantity'] * servicios[i]['price']);
			}
		}
		escribirMontoServicios();
	});


</script>
<script>
	/*
		Obtiene el monto total por platillo
	*/
	function obtenerCostoPlatillo(){
		const platillos 			= @json($platillos);
		const paquetes 				= @json($paquetes);
		let filaActual 				= document.getElementById("lista_platillos").rows[1];
		let platilloSeleccionado	= filaActual.querySelector('.platillos');
		let cantidadPlatillo		= filaActual.querySelector('.cantidad');
		let precioPlatillo 			= filaActual.querySelector('.precio');
		let montoTotalPlatillo 		= filaActual.querySelector('.monto');
		let option 					= filaActual.querySelector('.platillos').querySelector('option:checked');


		// Si selecciona un paquete llena los campos con sus valores
		if (option.getAttribute('data-type')) {
			console.log('PAQUETE: ' + option.value);
			paquetes.forEach( function(paquete, indice, array) {
				if(option.value == paquete.id){
					try {
						cantidadPlatillo.value 			= paquete.quantity;
						precioPlatillo.innerText		= numberToMoney(paquete.price);
						montoTotalPlatillo.innerText	= numberToMoney(paquete.price);
						
						platilloSeleccionado.setAttribute('name', 'paquete');
						cantidadPlatillo.setAttribute('onlyread', 'true');
						obtenerTotal();							
					} catch (error) {

					}
				}
			});
		}else{
			platilloSeleccionado.setAttribute('name', 'platillo');
			// Asigna los valores a los campos correspondientes
			platillos.forEach( function(platillo, indice, array) {
				if(platilloSeleccionado.value == platillo.id){
					try {
						precioPlatillo.innerText		= numberToMoney(platillo.price);
						montoTotalPlatillo.innerText	= numberToMoney(cantidadPlatillo.value * platillo.price);
						obtenerTotal();	
					} catch (error) {

					}
				}
			});
		}
	}
</script>

<script>
	/*
	Agrega complemento a tabla de menu
*/
function agregarComplemento(){
	// Si no hay separador lo agrega
	if(!(document.getElementById("separadorComplemento"))){
		document.getElementById("mytable").insertRow(-1).innerHTML = '<td colspan="5" id="separadorComplemento" class="separador">Complementos</td>';
		document.getElementById("separadorComplemento").parentNode.classList.add("filaSeparador");
	}

	// Agrega la fila
	if(document.getElementsByClassName('complementos').length <= 1){
		let tabla			= document.getElementById("lista_platillos");
		let separador		= document.getElementById("separadorComplemento").parentNode;
		let nuevaFila		= document.createElement('tr');

		nuevaFila.innerHTML = '<td width="70%" style="height: 150px;"> <select class="complementos" style="width: 100%;" name="complementos[]"> @foreach ($complementos as $complemento) <option data-img_src="{{asset($complemento->url_image)}}" value="{{$complemento->id}}" class="platillosReservacion optionComplemento">{{$complemento->name}} $ Sin descripción</option> @endforeach <option value="-1" class="text-muted" selected disabled >Selecciona un complemento</option> </select> </td>'+
									'<td width="10%"><input disabled type="number" class="form-control mb-md-1 mb-5 text-right" value=1 min="1"></td>'+
									'<td width="10%" align="right" class="text-success font-weight-bold">¡Gratis!</td>'+
									'<td width="10%" align="right" class="text-success font-weight-bold">¡Gratis!</td>'+
									'<td width="10%"><a class="action-btns1" title="Remover" onclick="eliminarComplemento()"><i class="feather feather-x text-danger"></i></a></td>';
		
		tabla.insertBefore(nuevaFila, separador.nextSibling);
	}
		
	// Aplica estilos al Select agregado
	var options = {
		'templateSelection': custom_template,
		'templateResult': custom_template,
	}
	$('.complementos').select2(options);

	let selectsPlatillos = document.getElementsByClassName('complementos');

	for (let index = 0; index < $('.complementos').length; index++) {
		selectsPlatillos[index].nextSibling.classList.add("carclass");
	}
}
</script>

<script>
/*
	Agrega servicio a tabla de menu
*/
function agregarServicio(){
	if(document.getElementsByClassName('servicios').length < @json($servicios).length){
		// Si no hay separador lo agrega
		if(!(document.getElementById("separadorServicios"))){
			document.getElementById("mytable").insertRow(-1).innerHTML = '<td colspan="5" id="separadorServicios" class="separador">Servicios</td>';
			document.getElementById("separadorServicios").parentNode.classList.add("filaSeparador");
		}
		let tabla			= document.getElementById("lista_platillos");
		let separador		= document.getElementById("separadorServicios").parentNode;
		let nuevaFila		= document.createElement('tr');
		nuevaFila.innerHTML = '<td width="70%" style="height: 150px;"> <select onchange="obtenerCostoServicio(event);" class="servicios" style="width: 100%;" name="servicios[]"> @foreach ($servicios as $servicio) <option data-img_src="{{asset($servicio->url_img)}}" value="{{$servicio->id}}" class="platillosReservacion optionComplemento">{{$servicio->name}} $ {{$servicio->description}}</option> @endforeach <option value="-1" class="text-muted" selected disabled >Selecciona un servicio</option> </select> </td>'+
									'<td width="10%"><input type="number" class="form-control mb-md-1 mb-5 cantidad" value=1 min="1" onchange="obtenerCostoServicio(event);" onkeyup="obtenerCostoServicio(event);" align="right" name="quantityServices[]"></td>'+
									'<td width="10%" class="precio" align="right">$0.00</td>'+
									'<td width="10%" class="monto" align="right">$0.00</td>'+
									'<td width="10%"><a class="action-btns1" title="Remover" onclick="eliminarServicio()"><i class="feather feather-x text-danger"></i></a></td>';

    	tabla.insertBefore(nuevaFila, separador.nextSibling);
    }
		

	// Aplica estilos al Select agregado
	var options = {
		'templateSelection': custom_template,
		'templateResult': custom_template,
	}
	$('.servicios').select2(options);

	let selectsServicios = document.getElementsByClassName('servicios');

	for (let index = 0; index < $('.servicios').length; index++) {
		selectsServicios[index].nextSibling.classList.add("carclass");
	}
}



/*
	Obtiene el monto total por servicio
*/
function obtenerCostoServicio(event){
	const servicios				= @json($servicios);
	let filaActual				= event.target.parentNode.parentNode;
	let servicioSeleccionado	= filaActual.querySelector('.servicios').value;
	let cantidadServicio		= filaActual.querySelector('.cantidad').value;
	let precioServicio			= filaActual.querySelector('.precio');
	let montoTotalServicio		= filaActual.querySelector('.monto');

	// Asigna los valores a los campos correspondientes
	servicios.forEach( function(servicio, indice, array) {
		if(servicioSeleccionado == servicio.id){
			try {
				precioServicio.innerText		= numberToMoney(servicio.price);
				montoTotalServicio.innerText 	= numberToMoney(cantidadServicio * servicio.price);
				obtenerTotal();	
			} catch (error) {
				
			}
		}
	});
}
</script>
{{-- Popup loading --}}
<script type="text/javascript">
document.getElementById("enviar")
  .addEventListener('click', (event) => {
	Swal.fire({
        title: 'Por favor espera !',
        html: 'Estamos guardando la reservación',
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        },
    });
  });
</script>
@endsection
