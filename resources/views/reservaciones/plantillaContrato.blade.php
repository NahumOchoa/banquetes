<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>

		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta content="Banquetes Cristy" name="author">


        <!--Favicon -->
		<link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon"/>
		<!-- Title -->
		<title>Banquetes Cristy</title>

		<style>
            *{
                font-family: Arial, Helvetica, sans-serif;
            }
            
            #logo{
                width: 15%;
                background-color: #fff;
            }

            #logo img{
                width: 100%;
            }

            p{
                text-align: justify;
            }
            span, .valoresDinamicos{
                text-decoration: underline;
                text-transform: uppercase;
                font-weight: bold;
            }
        </style>
	</head>


    <header>
        <div id="logo">
            <img src="{{ asset('images/logo_factura.png') }}" >
        </div>
        <div style="text-align: center;">
            <p style="text-align: center; line-height: 160%">PARA TODO EVENTO SOCIAL <br>
                QUINCEANERAS, BODAS <br>
                GRADUACIONES, BAUTIZOS</p>
        </div>
    </header>

	<body>
        <div id="informacionDeContacto">
            <table style="border: 0px; width: 800px;">
                <tr>
                    <td width="250px">Tel: 31 2 54 31</td>
                    <td width="250px">Celular: 834 138 7139</td>
                    <td width="250px">FB: Banquetes Cristy</td>
                </tr>
            </table>
        </div>

        <p>Sitio web: <a href="https://jmonrealr.wixsite.com/banquetes">https://jmonrealr.wixsite.com/banquetes</a> </p>

        <br>

        <h3 style="text-align: center">CONTRATO DE SERVICIOS</h3>

        <p>
            EL SENOR (A) <span>{{$booking->user->name}}</span> CON 
            @if ($profile)
                TELEFONO <span>{{$profile->phone_number}}</span>.    
            @else
                correo <span>{{$booking->user->email}}</span>.
            @endif
            
        </p>
        <p>
            COMO CONTRATANTE Y POR LA OTRA PARTE LA SRA. CRISTINA ROMERO VAZQUEZ, COMO PRESTADOR DEL SERVICIO, SE COMPROMETEN A RESPETAR LOS ACUERDOS ESTIPULADOS EN ESTE CONTRATO, DEJANDO CUBIERTO LOS SENALAMIENTOS DESCRITOS EN EL MISMO.
        </p>
        {{-- <p> --}}
            <table>
                <tr>
                    <td>PLATILLOS</td>
                    <td align="right" class="valoresDinamicos">{{$booking->quantity}}</td>
                </tr>
                <tr>
                    <td>MESEROS</td>
                    <td align="right" class="valoresDinamicos">
                        @if (
                                $services->contains(function ($service, $i) {
                                    if ($service['name'] == 'Mesero') {
                                        print($service->pivot->quantity);
                                        return true;
                                    }
                                })
                            )
                        @else
                            NO APLICA
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>LOZA</td>
                    <td align="right" class="valoresDinamicos">
                        @if (
                                $services->contains(function ($service, $i) {
                                    if ($service['name'] == 'Loza') {
                                        print($service->pivot->quantity);
                                        return true;
                                    }
                                })
                            )
                        @else
                            NO APLICA
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>DESECHABLES</td>
                    <td align="right" class="valoresDinamicos">SI APLICA</td>
                </tr>
            </table>
            {{-- PLATILLOS _________ ________________ <br>
            MESEROS __________ ________________ <br>
            LOZA ______________ ________________ <br>
            DESECHABLES _____ ________________ --}}
        {{-- </p> --}}
        <p>
            AL TERMINAR LA REUNION HABRA RECUENTO DE LA LOZA UTILIZADA, HACIENDO UN CARGO ADICIONAL POR LA FALTANTE.
        </p>
        {{-- <p> --}}
            <table>
                <tr>
                    <td>VASOS</td>
                    <td align="right" class="valoresDinamicos">{{$booking->quantity}} unidades</td>
                </tr>
                <tr>
                    <td>PLATOS</td>
                    <td align="right" class="valoresDinamicos">{{$booking->quantity}} unidades</td>
                </tr>
                <tr>
                    <td>TENEDORES</td>
                    <td align="right" class="valoresDinamicos">{{$booking->quantity}} unidades</td>
                </tr>
                <tr>
                    <td>CUCHILLOS</td>
                    <td align="right" class="valoresDinamicos">{{$booking->quantity}} unidades</td>
                </tr>
            </table>
            
            <br>

            FECHA <span>{{$booking->booking_date}}</span> HORA <span>{{$booking->booking_time}}</span> <br>
            DOMICILIO (SALON) <span>{{$booking->address->street_name}}. México, {{$booking->address->zip_code}} Cd Victoria, Tamaulipas.</span>
        {{-- </p> --}}
        <p>
            Al firmar este contrato el contratante cubrirá el costo del servicio en un 65%, el 35% restante, así como los utensilios faltantes deberán ser cubiertos al término de la reunión.
        </p>
        <br>
        <p style="text-align: center;">
            ________________________________ <br>
            Nombre y firma del contratante
        </p>
    </body>
</html>