@extends('app')

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
@endsection

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Reservaciones</h4>
    </div>
    <div class="page-rightheader ml-md-auto">
        <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
            <div class="btn-list">
                <a href="reservaciones/create" class="btn btn-primary mr-3">
                    <i class="feather  feather-plus sidemenu_icon"></i>
                    Agregar reservaciones</a>
            </div>
        </div>
    </div>
</div>
<!--End Page header-->

<!-- Row -->
<div class="row">
    @include('sweetalert::alert')
    <div class="col-xl-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header  border-0">
                <h4 class="card-title">Lista de Reservaciones</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    {{--DATATABLE--}}
                    <table class="table  table-vcenter text-wrap table-bordered border-bottom" id="project-list">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre del evento</th>
                                <th>Dirección</th>                                
                                <th>Fecha y hora</th>
                                <th>Cliente</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($bookings)
                                @foreach ($bookings as $booking)
                                    <tr>
                                        <td>{{$booking->id}}</td>
                                        {{-- <td width="200px"> --}}
                                        <td>
                                            <span>{{$booking->name}}</span>
                                        </td>
                                        {{-- <td width="350px"> --}}
                                        <td>
                                            <span>{{$booking->address->street_name}}. México, {{$booking->address->zip_code}} Cd Victoria, Tamaulipas.</span>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <div class="mr-3 mt-0 mt-sm-1 d-block">
                                                    <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Fecha:</span>{{$booking->booking_date}}</h6>
                                                    <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Hora:</span> {{$booking->booking_time}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <span class="avatar avatar-md brround mr-3" style="background-image: url({{asset('images/usuario.png')}}"></span>
                                                <div class="mr-3 mt-0 mt-sm-1 d-block">
                                                    <h6 class="mb-1 fs-14">{{$booking->user->name}}</h6>
                                                    <p class="text-muted mb-0 fs-12">{{$booking->user->email}}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <select name="status" id="" onchange="updateEstatus(event);" data-id="{{$booking->id}}" class="badge bg-danger-transparent border border-danger">
                                                @foreach ($statuses as $status)
                                                @if ($status->id == $booking->status_id)
                                                    <option value="{{$status->id}}" selected>{{$status->name}}</option>
                                                @else
                                                    <option value="{{$status->id}}">{{$status->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('reservaciones.show', $booking->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                                <a href="{{ route('reservaciones.edit', $booking->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                                <form action="{{route('reservaciones.destroy', $booking->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="action-btns1 feather feather-trash-2 text-danger show_confirm" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                    {{--FIN DATATABLE--}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection

@section('extra-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
{{--"url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/es-MX.json"//ES = 90% and MX 88% //cdn.datatables.net/plug-ins/1.11.4/i18n/es-MX.json--}}
<script type="text/javascript">
    $(document).ready( function () {
         $("#project-list").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/es-mx.json"
            }
        });
    });
    $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          event.preventDefault();
          swal({
              title: "¿Esta seguro de eliminar esta reservación?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
              buttons: ["Cancelar", "Eliminar"],
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>


<script>
    function updateEstatus(event) {
        let status = event.target.value;
        let id = event.target.getAttribute('data-id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{route('update-status')}}",
            data: {
                'status': status,
                'id': id,
            },
            success: function(response) {
                Toast.fire({
                  icon: 'success',
                title: response.mensaje
                })
            }
        });
    }


    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2000,
      timerProgressBar: true,
    })


</script>

@endsection
