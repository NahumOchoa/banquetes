@extends('app')

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Configuración</h4>
    </div>
</div>
<!--End Page header-->

<!-- Row -->
<div class="row">
    {{-- Tarjeta submenu --}}
    {{-- <div class="col-xl-3 col-md-12 col-lg-12">
        <div class="card">
            <div class="table-responsive">
                <ul class="side-menu">
                    <li class="slide">
                        <a class="side-menu__item" href="#perfil">
                            <i class="feather  feather-user sidemenu_icon"></i>
                            <span class="side-menu" >Ajustes de cuenta</span>
                        </a>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item" href="#pagos">
                            <i class="feather  feather-credit-card sidemenu_icon"></i>
                            <span class="side-menu">Modos de pago</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}

    {{-- Tarjeta Información usuario --}}
    <div class="col-xl-12 col-md-12 col-lg-12" id="perfil">
        <div class="card">
            <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <h4 class="mb-5 font-weight-semibold">Información básica</h4>

                    {{-- Fila foto de perfil --}}
                    <div class="row">

                        <div class="col-xl-7">
                            <div class="box-widget widget-user">
                                <div class="widget-user-image d-sm-flex">
                                    <div class="user-pic">
                                        @if(is_null($profile->url_image))
                                            <img src="{{asset('images/usuario.png')}}" id="image" alt="user-img" class="avatar-xxl rounded-circle mb-1">
                                        @else
                                            <img src="{{ asset($profile->url_image) }}" id="image" alt="user-img" class="avatar-xxl rounded-circle mb-1">
                                        @endif
                                    </div>
                                    <div class="ml-sm-4 mt-4">
                                        <div class="form-group">
                                            <label class="form-label">Foto de perfil</label>
                                            <div class="input-group file-browser">
                                                <input type="text" class="form-control border-right-0 browse-file" placeholder="Cargar otra imagen" readonly value="@isset($profile->url_image){{substr(explode('/', $profile->url_image)[1],10)}}@endisset">
                                                <label class="input-group-append mb-0">
                                                    <span class="btn ripple btn-primary">
                                                        Examinar <input type="file" class="file-browserinput" style="display: none;" multiple=""
                                                                        name="url_image" id="url_image" onchange="previewImage(event)">
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Fila Nombre usuario y Correo --}}
                    <div class="row">
                        {{-- Nombre  --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name" class="form-label">Nombre:</label>
                                <input id="first_name" class="form-control @error('first_name') is-invalid @enderror"
                                       type="text" placeholder="Ingresa tu nombre" name="first_name" autofocus
                                       value="{{ old('first_name') }}@isset($profile){{$profile->first_name}}@endisset">
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name" class="form-label">Apellidos:</label>
                                <input id="last_name" class="form-control @error('last_name') is-invalid @enderror"
                                       type="text" placeholder="Ingresa tus apellidos" name="last_name" autofocus
                                       value="{{ old('last_name') }}@isset($profile){{$profile->last_name}}@endisset">
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{-- Nombre  --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone_number" class="form-label">Telefono:</label>
                                <input id="phone_number" class="form-control @error('phone_number') is-invalid @enderror"
                                       type="text" placeholder="Ingresa tu numero telefonico" name="phone_number" autofocus
                                       value="{{ old('phone_number') }}@isset($profile){{$profile->phone_number}}@endisset">
                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gender" class="form-label">Genero:</label>
                                <select id="gender" name="gender" class="form-control select2">
                                    <option></option>
                                    <option value="Masculino" @isset($profile->gender){{ ($profile->gender == 'Masculino') ? 'selected' : '' }}@endisset>Masculino</option>
                                    <option value="Femenino"@isset($profile->gender){{ ($profile->gender == 'Femenino') ? 'selected' : '' }}@endisset>Femenino</option>
                                    <option value="Otro"@isset($profile->gender){{ ($profile->gender == 'Otro') ? 'selected' : '' }}@endisset>Otro</option>
                                    <option value="Prefiero no decir"@isset($profile->gender){{ ($profile->gender == 'Prefiero no decir') ? 'selected' : '' }}@endisset>Prefiero no decir</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    {{-- Button Guardar--}}
                    <div class="card-footer text-right">
						<button type="submit" class="btn btn-primary">
							<i class="feather  feather-save sidemenu_icon"></i>
							Guardar
                        </button>
				    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Row-->


<!-- Row -->
{{-- <div class="row" id="pagos">
    <div class="col-xl-3 col-md-12 col-lg-12">
    </div>

    <div class="col-xl-9 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header d-xl-flex d-block">
                <div class="card-leftheader">
                    <h4 class="card-title" id="impuestos">Modos de pago</h4>
                </div>
                <div class="card-rightheader ml-md-auto">
                    <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
                        <div class="btn-list">
                            <a href="#" class="btn btn-outline-primary mr-3">
                                <i class="feather  feather-plus sidemenu_icon"></i>
                                Agregar modo de pago</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-vcenter text-nowrap border-top mb-0 invoice-table">
                            <thead>
                                <tr>
                                    <th class="wd-10p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nombre</font></font></th>
                                    <th class="wd-15p border-bottom-0" width="150px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Acciones</font></font></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>Tarjeta de credito</span>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                            <button class="action-btns1" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"><i class="feather feather-trash-2 text-danger"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Efectivo</span>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                            <button class="action-btns1" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"><i class="feather feather-trash-2 text-danger"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!-- End Row-->
@endsection

@section('extra-script')
    <script type="text/javascript">
        const previewImage = e => {
            const preview = document.getElementById('image');
            preview.src = URL.createObjectURL(e.target.files[0]);
            preview.onload = () => URL.revokeObjectURL(preview.src);
        };
        $(function () {
            $(".select2").select2({
                placeholder: "Selecciona tu genero",
            });
        });
    </script>
@endsection

