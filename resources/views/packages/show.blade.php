@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver Paquete {{ $package->id }}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('packages.index') }}">Paquetes</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver Paquetes</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-6 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-header d-xl-flex d-block">
                <h4 class="mb-5 font-weight-semibold">Información del Paquete</h4>
            </div>
			<div class="card-body">
                <div class="form-group">
                    <label for="name" class="col-md-4 col-form-label">Platillo: </label>
                    <span class="badge bg-primary-transparent">{{ $package->menu->name }}</span>
                </div>
                <div class="form-group">
                    <label for="description" class="col-md-4 col-form-label">Descripción: </label>
                    <span class="mb-2 fs-14">{{ $package->description }}</span>
                </div>
                <div class="form-group">
                    <label for="price" class="col-md-4 col-form-label">Precio</label>
                    <span class="mb-2 fs-14">$ {{ number_format($package->price, 2) }}</span>
                </div>
                <div class="form-group">
                    <label for="quantity" class="col-md-4 col-form-label">Cantidad: </label>
                    <span class="mb-2 fs-14">{{ $package->quantity }}</span>
                </div>

			</div>
            <div class="card-footer text-right">
				<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
			</div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection
