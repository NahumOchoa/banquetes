@csrf

    <div class="form-group row">
        <label for="menu_id" class="col-md-4 col-form-label">Selecciona un platillo*</label>
        <div class="col-md-6">
            <select class="form-control select2" id="menu_id" name="menu_id">
            @isset($menus)
                @foreach($menus as $menu)
                    <option value="{{$menu->id}}"
                        @isset($edit){{ ($menu->id == $package->menu->id) ? 'selected' : ''}}@endisset>
                        {{$menu->name}}
                    </option>
                @endforeach
            @endisset
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label">Descripción*</label>
        <div class="col-md-6">
            <input id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                   name="description" value="{{ old('description') }}@isset($package){{$package->description}}@endisset"
                   required autocomplete="package" placeholder="Ingresa una descripción del paquete">
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-4 col-form-label" for="quantity">Cantidad*</label>
        <div class="col-md-6">
            <input class="form-control @error('quantity') is-invalid @enderror" type="number" placeholder="10" min="10"
                   name="quantity" id="quantity" step="1" max="100" value="@isset($edit){{$package->quantity}}@endisset"
                    required>
            @error('quantity')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
            <label class="col-md-4 col-form-label" for="price">Precio*</label>
            <div class="input-group col-md-6">
                <span class="input-group-text">$</span>
                <input class="form-control @error('price') is-invalid @enderror" type="number" placeholder="0.00" min="0"
                    name="price" id="price" step="0.01" value="@isset($edit){{$package->price}}@endisset"
                    required>
{{--                <span class="input-group-text">.00</span>--}}
                @error('price')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
    </div>

