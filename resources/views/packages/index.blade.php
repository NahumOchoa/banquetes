@extends('app')

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
@endsection

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Paquetes</h4>
    </div>
    <div class="page-rightheader ml-md-auto">
        <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
            <div class="btn-list">
                <a href="{{ route('packages.create') }}" class="btn btn-primary mr-3">
                    <i class="feather  feather-plus sidemenu_icon"></i>
                    Agregar Paquete</a>
            </div>
        </div>
    </div>
</div>
<!--End Page header-->


{{-- ////////////// S O L O   C A M B I A R   L O S   V A L O R E S   N E C E S A R I O S ////////////// --}}

<!-- Row -->
<div class="row">
    @include('sweetalert::alert')
    <div class="col-xl-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header  border-0">
                <h4 class="card-title">Lista de Paquetes</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-vcenter text-wrap table-bordered border-bottom" id="package-list">
                        <thead>
                            <tr>
                                <th class="border-bottom-0">ID</th>
                                <th class="border-bottom-0">Platillo</th>
                                <th class="border-bottom-0">Descripción</th>
                                <th class="border-bottom-0">Cantidad</th>
                                <th class="border-bottom-0">Precio</th>
                                <th class="border-bottom-0">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($packages)
                                @foreach($packages as $package)
                                    <tr>
                                        <td> {{ $package->id }}</td>
                                        <td>
                                            <span>{{ $package->menu->name }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $package->description }}</span>
                                        </td>
                                        <td style="text-align: right">
                                            <span>{{ $package->quantity }}</span>
                                        </td>
                                        <td style="text-align: right">
                                            <span>${{ number_format($package->price, 2) }}</span>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('packages.show', $package->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                                <a href="{{ route('packages.edit', $package->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                                <form action="{{ route('packages.destroy', $package->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="action-btns1 feather feather-trash-2 text-danger show_confirm" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"></button>
                                                </form>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection
@section('extra-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready( function () {
             $("#package-list").DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/es-mx.json"
                }
            });
        });
        $('.show_confirm').click(function(event) {
            let form = $(this).closest("form");
            event.preventDefault();
            swal({
                title: "¿Esta seguro de eliminar este paquete?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Eliminar"],
            })
            .then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
