@csrf
    {{-- NOMBRE --}}
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label">{{ __('Username') }}*</label>
        <div class="col-md-6">
            <input id="namemy" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}@isset($user){{$user->name}}@endisset" required autocomplete="name" autofocus>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    {{-- EMAIL --}}
    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}*</label>
        <div class="col-md-6">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}@isset($user){{$user->email}}@endisset" required autocomplete="email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    {{-- CONTRASEÑA --}}
@isset($create)
    <div class="form-group row">
        <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}*</label>
        <div class="col-md-6">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Confirm Password') }}*</label>
        <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>
    </div>
@endisset
    {{-- ROLES --}}
    <div class="form-group row">
        <label class="col-md-4 col-form-label">{{ __('Role') }}</label>
        @foreach ($roles as $role)
            <div class="form-check">
                <label for="{{ $role->name }}" class="col-md-4 col-form-label"></label>
                <input class="form-check-input" name="roles[]" type="checkbox" value="{{$role->id}}" id="{{$role->name}}"
                       @isset($user) @if (in_array($role->id, $user->roles->pluck('id')->toArray())) checked @endif @endisset>
                <label class="form-check-label" for="{{$role->name}}">
                    {{$role->name}}
                </label>
                </div>
        @endforeach
    </div>
