@extends('app')

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
@endsection

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Usuarios</h4>
    </div>
    <div class="page-rightheader ml-md-auto">
        <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
            <div class="btn-list">
                <a href="{{ route('admin.users.create') }}" class="btn btn-primary mr-3">
                    <i class="feather  feather-plus sidemenu_icon"></i>
                    Agregar Usuario</a>
            </div>
        </div>
    </div>
</div>
<!--End Page header-->


{{-- ////////////// S O L O   C A M B I A R   L O S   V A L O R E S   N E C E S A R I O S ////////////// --}}

<!-- Row -->
<div class="row">
    @include('sweetalert::alert')
    <div class="col-xl-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header  border-0">
                <h4 class="card-title">Lista de Usuarios</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    {{--DATATABLE--}}
                    <table class="table table-vcenter text-wrap table-bordered border-bottom" id="user-list">
                        <thead>
                            <tr>
                                <th class="border-bottom-0">ID</th>
                                <th class="border-bottom-0">{{ __('Username') }}</th>
                                <th class="border-bottom-0">{{ __('E-Mail Address') }}</th>
                                <th class="border-bottom-0">{{ __('Role') }}</th>
                                <th class="border-bottom-0">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($users)
                                @foreach($users as $user)
                                    <tr>
                                        <td> {{ $user->id }}</td>
                                        <td>
                                            <span>{{ $user->name }}</span>
                                        </td>
                                        <td>
                                            <span>{{ $user->email }}</span>
                                        </td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                @switch($role->name)
                                                    @case('Administrador')
                                                        <span class="badge bg-danger-transparent">{{ $role->name }}</span>
                                                        @break
                                                    @case('Empleado')
                                                        <span class="badge bg-success-transparent">{{ $role->name }}</span>
                                                        @break
                                                    @default
                                                        <span class="badge bg-primary-transparent">{{ $role->name }}</span>
                                                @endswitch
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('admin.users.show', $user->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                                <a href="{{ route('admin.users.edit', $user->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                                <form action="{{ route('admin.users.destroy', $user->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="action-btns1 feather feather-trash-2 text-danger show_confirm" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"></button>
                                                </form>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                    {{--FIN DATATABLE--}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection
@section('extra-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready( function () {
             $("#user-list").DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/es-mx.json"
                }
            });
        });
        $('.show_confirm').click(function(event) {
            let form = $(this).closest("form");
            event.preventDefault();
            swal({
                title: "¿Esta seguro de eliminar este usuario?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Eliminar"],
            })
            .then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
