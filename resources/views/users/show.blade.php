@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver Usuario {{ $user->id }}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver Usuario</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-6 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-header d-xl-flex d-block">
                <h4 class="mb-5 font-weight-semibold">Información del Usuario</h4>
            </div>
			<div class="card-body">
                <div class="form-group">
                    <label for="name" class="col-md-4 col-form-label">{{ __('Username') }}</label>
                    <span class="mb-2 fs-14">{{ $user->name }}</span>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>
                    <span class="mb-2 fs-14">{{ $user->email }}</span>
                </div>
                <div class="form-group">
                    <label for="roles" class="col-md-4 col-form-label">{{ __('Role') }}</label>
                    @foreach($user->roles as $role)
                        @switch($role->name)
                            @case('Administrador')
                                <span class="badge bg-danger-transparent">{{ $role->name }}</span>
                                @break
                            @case('Empleado')
                                <span class="badge bg-success-transparent">{{ $role->name }}</span>
                                @break
                            @default
                                <span class="badge bg-primary-transparent">{{ $role->name }}</span>
                        @endswitch
                    @endforeach
                </div>

			</div>
            <div class="card-footer text-right">
				<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
			</div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection
