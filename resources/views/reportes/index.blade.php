@extends('app')

@section('contenido')

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <meta name="csrf-token" content="{{csrf_token() }}">
</head>
<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Reporte de Ingresos</h4>
        <ul class="breadcrumb">
            <li class="mb-1 fs-16">Reportes</li>
            <li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
            <li class="text-muted mb-1 fs-16">Reporte de Ingresos</li>
        </ul>
    </div>
</div>
<!--End Page header-->

<!-- Row -->
<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <form method="POST" action="create-pdf-file" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="card-body">
                    <h4 class="mb-5 font-weight-semibold">Rango del Reporte</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Main col -->
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label class="form-label">
                                            A partir de:
                                        </label>
                                        <div class='input-group date' id='CalendarDateTime'>
                                            <input type='text' class="form-control" name="from_date" id="from_date"/>
                                            <span class="input-group-addon"><span class="feather feather-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label class="form-label">
                                            Hasta:
                                        </label>
                                        <div class='input-group date' id='CalendarDateTime'>
                                            <input type='text' class="form-control" name="to_date" id="to_date"/>
                                            <span class="input-group-addon"><span class="feather feather-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">
                        <i class="feather feather-file-text sidemenu_icon"></i>
                        Generar Reporte
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-5 font-weight-semibold">Previsualización</h4>
                <div class="row">
                    <embed name="pdf" id="pdf" src="{{asset('contratoBanquetes.pdf')}}" type="application/pdf" height="600px" />
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'mm-dd-yyyy'
    });
</script>
<!-- End Row-->
@endsection

<!-- <script>
    $(document).ready(function() {
        var form = '#form';
        $(form).on('submit', function(e) {
            e.preventDefault();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            $.ajax({
                type: "POST",
                success: function(data) {
                    // Embed pdf
                    $('#pdf').attr("src", data);
                }
            });
        });
    });
</script> -->