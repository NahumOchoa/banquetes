<style>
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;
  height: 29.7cm;
  margin: 0 auto;
  color: #001028;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: #2b5876;
    background: -webkit-linear-gradient(to right, #4e4376, #2b5876);
    background: linear-gradient(to right, #4e4376, #2b5876);
}


#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.total {
  font-size: 1.2em;
}

.grand .total{
    text-align: left;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
.box {
  border: 1px solid #333333;
  width: 500px;
  height: 100px;
  overflow: scroll;
}

</style>




<!DOCTYPE html>
<html>

<head>
    <title>Reporte de Ingresos</title>
</head>

<body>
<header class="clearfix">
    <h1>{{ $title }}</h1>
    <div id="project">
        <div><span>PROJECT</span> Website Banquetes Crisy</div>
        <div><span>FECHA</span> De {{ $from_date }}   hasta   {{ $to_date }} </div>
        <div><a href="">Banquetescristy@gmail.com</a></div>
    </div>
</header>
<main>
    <div class="align">
        <table class="box">
            <thead>
                    <tr style="background: #2b5876;  /* fallback for old browsers */
                                background: -webkit-linear-gradient(to right, #4e4376, #2b5876);  /* Chrome 10-25, Safari 5.1-6 */
                                background: linear-gradient(to right, #4e4376, #2b5876); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */">
                        <th class="service" Style="color: #fff;">INGRESOS</th>
                        <th class="desc" Style="color: #fff; text-align:right;">CANTIDAD MXN</th>
                    </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="service">Paquetes Parcialmente Pagadas</td>
                    <td class="total"><strong>{{ $parcially_paid }}</strong></td>
                </tr>
                <tr>
                    <td class="service">Paquetes Totalmente Pagados</td>
                    <td class="total"><strong>{{ $totally_paid }}</strong></td>
                </tr>
                <tr>
                    <td class="service">Ingresos Antes de Impuestos</td>
                    <td class="total"><strong>{{ $income_before_taxes }}</strong></td>
                </tr>
                <tr>
                    <td class="service">Ingresos</td>
                    <td class="total"><strong>{{ $income }}</strong></td>
                </tr>

                <tr>
                    <td class="service">Cuentas por cobrar</td>
                    <td class="total"><strong>{{ $accounts_receivables }}</strong></td>
                </tr>
            </tbody>
        </table>
    </div>
</main>
</body>
</html>
