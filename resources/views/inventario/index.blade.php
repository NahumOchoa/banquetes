@extends('app')

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
@endsection

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="page-rightheader ml-md-auto">
        <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
            <div class="btn-list">
                <a href="{{ route('inventario.create') }}" class="btn btn-primary mr-3">
                    <i class="feather  feather-plus sidemenu_icon"></i>
                    Agregar producto</a>
            </div>
        </div>
    </div>
</div>
<!--End Page header-->

    <!-- Row -->
    <div class="row">
        @include('sweetalert::alert')
    	<div class="col-xl-12 col-md-12 col-lg-12">
    		<div class="card">
                <div class="card-header  border-0">
                    <h4 class="card-title">Lista de Inventario</h4>
                </div>
    			<div class="card-body">
                <div class="table-responsive">
                    <table class="table table-vcenter text-wrap table-bordered border-bottom" id="project-list">
						<thead>
							<tr>
                                <th class="border-bottom-0">ID</th>
                                <th class="border-bottom-0">Nombre</th>
                                <th class="border-bottom-0">Cantidad</th>
                                <th class="border-bottom-0">Descripción</th>
                                <th class="border-bottom-0">Creado en</th>
                                <th class="border-bottom-0">Acciones</th>
							</tr>
						</thead>
                        <tbody>
                            @isset($inventory)
                                    @foreach($inventory as $inv)
                                        <tr>
                                            <td>{{ $inv->id }}</td>
                                            <td>
                                                <div class="d-flex">
                                                    @if (is_null($inv->url_image))
                                                        <span class="avatar avatar-md brround mr-3" style="border-color: transparent; background-color:transparent; background-image: url({{'https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png'}}"></span>
                                                    @else
                                                    <span class="avatar avatar-md brround mr-3" style="border-color: transparent; background-color:transparent; background-image: url({{asset($inv->url_image)}}"></span>
                                                    @endif
                                                    <div class="mt-0 mt-sm-2 d-block">
                                                        <h6 class="mb-1 fs-17">{{ $inv->name }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="right">{{ $inv->quantity }}</td>
                                            <td>{{ $inv->description }}</td>
                                            <td>{{ date('d/M/Y', strtotime( $inv->created_at)) }}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="{{ route('inventario.show', $inv->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                                    <a href="{{route('inventario.edit', $inv->id)}}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                                    <form action="{{route('inventario.destroy', $inv->id)}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="action-btns1 feather feather-trash-2 text-danger show_confirm" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                            @endisset
                        </tbody>
                    </table>
					</div>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- End Row-->
@endsection

@section('extra-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
{{--"url": "//cdn.datatables.net/plug-ins/1.11.3/i18n/es-MX.json"//ES = 90% and MX 88% //cdn.datatables.net/plug-ins/1.11.4/i18n/es-MX.json--}}
<script type="text/javascript">
    $(document).ready( function () {
         $("#project-list").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/es-mx.json"
            }
        });
    });
    $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          event.preventDefault();
          swal({
              title: "¿Esta seguro de eliminar este producto?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
              buttons: ["Cancelar", "Eliminar"],
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>

@endsection
