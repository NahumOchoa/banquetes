@extends('app')

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
	<div class="page-leftheader">
		<h4 class="page-title">Ver Producto {{$inventory->id}}</h4>
		<ul class="breadcrumb">
			<li class="mb-1 fs-16"><a href="/inventario">Inventario</a></li>
			<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
			<li class="text-muted mb-1 fs-16">Ver Producto</li>
		</ul>
	</div>

</div>
<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-3 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-header d-xl-flex d-block">
				<div class="card-leftheader">
					<h4 class="card-title" id="impuestos">Información del Producto</h4>
				</div>
			</div>
			<div class="card-body text-center">
				<div class="user-pic">
					@if(is_null($inventory->url_image))
					<img src="https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png" alt="user-img" class="avatar-xxl rounded-circle mb-1" alt="Foto producto">
					@else
					<img src="{{ asset($inventory->url_image) }}" alt="user-img" class="avatar-xxl rounded-circle mb-1" alt="Foto producto">
					@endif
				</div>
				<div class="col-sm-12">
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Nombre</font>
						<span class="mb-2 fs-14">{{$inventory->name}}</span>
					</div>
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Descripción</font>
						<span class="mb-2 fs-14">{{$inventory->description}}</span>
					</div>
					<div class="row">
						<font style="vertical-align: inherit; font-weight:bold">Cantidad</font>
						<span class="mb-2 fs-14">{{$inventory->quantity}}</span>
					</div>
				</div>
			</div>
            <div class="card-footer text-right">
                <a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
                    <i class="feather feather-corner-down-left sidemenu_icon"></i>
                    Regresar
                </a>
            </div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection
