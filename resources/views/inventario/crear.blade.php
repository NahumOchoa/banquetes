@extends('app')

@section('contenido')

@include('sweetalert::alert')
<!--Page header-->
<div class="page-header d-xl-flex d-block">
	<div class="page-leftheader">
		@if(is_null($inventory))
		<h4 class="page-title">Agregar producto</h4>
		@else
		<h4 class="page-title">Editar Producto {{$inventory->id}}</h4>
		@endif
		<ul class="breadcrumb">
			<li class="mb-1 fs-16"><a href="{{ route('inventario.index') }}">Inventario</a></li>
			<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
			@if(is_null($inventory))
			<li class="text-muted mb-1 fs-16"> Agregar producto</li>
			@else
			<li class="text-muted mb-1 fs-16">Editar Producto</li>
			@endif
		</ul>
	</div>
</div>
<!--End Page header-->

<!-- ------------------------------------------------------------------------- -->
<!-- Row -->
<div class="row">
	<div class="col-xl-12 col-md-12 col-lg-12">
		<div class="card">
		@if(is_null($inventory))
		<form action="{{ route('inventario.store') }}" method="POST" enctype="multipart/form-data">
		@else
		<form action="{{ route('inventario.update', $inventory->id) }}" method="POST" enctype="multipart/form-data">
			@method('PUT')
		@endif
            @csrf
			<div class="card-body">
				<h4 class="mb-5 font-weight-semibold">Información básica</h4>
				<div class="row">
					<div class="col-sm-3">
						<!--left col-->
						<div class="text-center user-pic">
							@if(is_null($inventory) or is_null($inventory->url_image) or !file_exists(public_path() .'/'. $inventory->url_image))
							<img src="https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png" class="avatar-xxl rounded-circle mb-1" alt="Foto producto" name="image" id="image">
							@else
							<img src="{{ asset($inventory->url_image) }}" class="avatar-xxl rounded-circle mb-1" alt="Foto producto" name="image" id="image">
							@endif
							<h6>Suba una imagen...</h6>
							<label class="mb-0">
								<span class="btn ripple btn-primary">
									<i class="feather feather-upload sidemenu_icon"></i>
									Examinar <input type="file" class="file-browserinput" style="display: none;" accept="image/*" name="url_image" id="url_image" onchange="previewImage(event)">
								</span>
							</label>
						</div>
					</div>
					<div class="col-sm-9">
						<!-- Main col -->
						<div class="row">
							<div class="col-xs-3">
								<div class="form-group">
									<label class="form-label" for="name">
										Nombre*
									</label>
									@if(is_null($inventory))
									<input class="form-control @error('name') is-invalid @enderror" type="text" placeholder="Ingresa el nombre del producto" name="name" id="name" maxlength="55">
									@else
									<input class="form-control @error('name') is-invalid @enderror" type="text" placeholder="Ingresa el nombre del producto" name="name" id="name" value="{{$inventory->name}}" maxlength="55">
									@endif
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            {{ $message }}
                                        </span>
                                    @enderror
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group">
									<label class="form-label" for="quantity">
										Cantidad*
									</label>
									@if(is_null($inventory))
									<input class="form-control @error('quantity') is-invalid @enderror" type="number" placeholder="100" min="0" name="quantity" id="quantity" min="1" max="500">
									@else
									<input class="form-control @error('quantity') is-invalid @enderror" type="number" placeholder="100" min="0" name="quantity" id="quantity" min="1" max="500" value="{{$inventory->quantity}}">
									@endif
                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            {{ $message }}
                                        </span>
                                    @enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-11">
								<div class="form-group">
									<label class="form-label" for="description">
										Descripción*
									</label>
									@if(is_null($inventory))
									<input type="text" class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="Ingrese la descripción del producto"  title="Ingrese la descripción del producto" maxlength="250">
									@else
									<input type="text" class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="Ingrese la descripción del producto"  title="Ingrese la descripción del producto" value="{{$inventory->description}}" maxlength="250">
									@endif
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            {{ $message }}
                                        </span>
                                    @enderror
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer text-right">
				<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
				<button type="submit" class="btn btn-primary">
					<i class="feather feather-save sidemenu_icon"></i>
					Guardar
				</button>
			</div>
		</form>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection

<script type="text/javascript">
	const previewImage = e => {
		const preview = document.getElementById('image');
		preview.src = URL.createObjectURL(e.target.files[0]);
		preview.onload = () => URL.revokeObjectURL(preview.src);
	};
</script>



@section('extra-script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
	<script src="{{ asset('js/confirmExit.js') }}"></script>
	<script src="{{ asset('js/characterCounter.js') }}"></script>
@endsection
