@extends('app')

@section('contenido')
                        
        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Agregar cliente</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="/clientes">Clientes</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Agregar cliente</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

		<!-- Row -->
		<div class="row">
			{{-- COLUMNA DERECHA --}}
			<div class="col-xl-12 col-md-12 col-lg-12">
				<div class="card">
					<form>
					<div class="card-body">
						<h4 class="mb-5 font-weight-semibold">Información básica</h4>
						<div class="row">
							<div class="col-xl-7">
								<div class="box-widget widget-user">
									{{--FOTO--}}
									<div class="widget-user-image d-sm-flex">
										<span class="avatar" style="background-image: url({{asset('images/usuario.png')}})">
										</span>
										<div class="ml-sm-4 mt-4">
											<div class="form-group">
												<label class="form-label">Foto de perfil</label>
												<div class="input-group file-browser">
													<input type="text" class="form-control border-right-0 browse-file" placeholder="Cargar otra imagen" readonly="">
													<label class="input-group-append mb-0">
														<span class="btn ripple btn-primary">
															Examinar <input type="file" class="file-browserinput" style="display: none;" multiple="" name="foto">
														</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						{{-- DATOS --}}
						<div class="row">
							<div class="col-md-6">
								{{-- NOMBRE --}}
								<div class="form-group">
									<label class="form-label">Nombre</label>
									<input class="form-control" type="text" placeholder="Ingresa el nombre del cliente" name="nombre" maxlength="22">
								</div>
							</div>
							<div class="col-md-6">
								{{-- APELLIDO --}}
								<div class="form-group">
									<label class="form-label">Apellidos</label>
									<input class="form-control" type="text" placeholder="Ingresa los apellidos del cliente" name="apellidos" maxlength="30">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								{{-- TELEFONO --}}
								<div class="form-group">
									<label class="form-label">Número telefonico</label>
									<input class="form-control" type="text" placeholder="Ingresa su número de telefono" name="telefono" maxlength="16">
								</div>
							</div>
							<div class="col-md-6">
								{{-- GENERO --}}
								<div class="form-group">
									<label class="form-label mb-0 mt-2">Género</label>
									<select name="genero" class="form-control custom-select select2" data-placeholder="Selecciona género">
										<option label="Selecciona género"></option>
										<option value="1">Femenino</option>
										<option value="2">Masculino</option>
									</select>
								</div>
							</div>
						</div>

						{{-- DIRECCION --}}
						<h4 class="mb-5 mt-7 font-weight-bold">Dirección</h4>

						<div class="row">
							<div class="col-md-6">
								{{-- ESTADO --}}
								<div class="form-group">
									<label class="form-label">Estado</label>
									<input class="form-control" type="text" placeholder="Ingresa el estado en que reside" name="estado" maxlength="20">
								</div>
							</div>
							<div class="col-md-6">
								{{-- CIUDAD --}}
								<div class="form-group">
									<label class="form-label">Ciudad</label>
									<input class="form-control" type="text" placeholder="Ingresa la ciudad" name="ciudad" maxlength="16">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								{{-- CALLE	 --}}
								<div class="form-group">
									<label class="form-label">Calle</label>
									<textarea rows="3" class="form-control" placeholder="Ingresa la calle" name="calle" maxlength="250"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								{{-- CODIGO POSTAL --}}
								<div class="form-group">
									<label class="form-label">C.P</label>
									<input class="form-control" type="text" placeholder="Ingresa el código postal" name="cp" maxlength="5">
								</div>
							</div>
						</div>
						
						
						<h4 class="mb-5 mt-7 font-weight-bold">Cuenta de ingreso</h4>
                            <div class="form-group">
                                <div class="row">
									{{-- CORREO --}}
                                    <div class="col-md-3">
                                        <label class="form-label mb-0 mt-2">Correo de empleado</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" name="email" placeholder="example@email.com">
                                    </div>
                                </div>
                            </div>
							{{-- CONTRASEÑA --}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label mb-0 mt-2">Contraseña</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" placeholder="Introduce una contraseña">
                                    </div>
                                </div>
                            </div>
					</div>
					<div class="card-footer text-right">
						<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
							<i class="feather feather-corner-down-left sidemenu_icon"></i>
							Regresar
						</a>
						<button type="submit" class="btn btn-primary">
							<i class="feather  feather-save sidemenu_icon"></i>
							Guardar</button>
					</div>
				</form>
				</div>
			</div>
		</div>
		<!-- End Row-->
@endsection

@section('extra-script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
	<script src="{{ asset('js/confirmExit.js') }}"></script>
	<script src="{{ asset('js/characterCounter.js') }}"></script>
@endsection