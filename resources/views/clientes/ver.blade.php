@extends('app')

@section('contenido')
                        
        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver cliente #1</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="/clientes">Clientes</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver cliente</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

	<!-- Row -->
	<div class="row">
	    {{-- Tarjeta información --}}
	    <div class="col-xl-3 col-md-12 col-lg-12">
	        <div class="card">
				<div class="card-header d-xl-flex d-block">
					<div class="card-leftheader">
						<h4 class="card-title" id="impuestos">Información del cliente</h4>
					</div>
				</div>
				
	        </div>
	    </div>

{{-- Tarjeta Sus reservaciones --}}
<div class="col-xl-9 col-md-12 col-lg-12">
	<div class="card">
		<div class="card-header d-xl-flex d-block">
			<div class="card-leftheader">
				<h4 class="card-title" id="impuestos">Sus reservaciones</h4>
			</div>
		</div>
		
		<div class="card-body">
			{{-- Tabla reservaciones --}}
			<div class="row">
				<div class="table-responsive">
					<table class="table table-vcenter text-wrap border-top mb-0 invoice-table">
                        <thead>
                            <tr>
                                <th class="wd-10p border-bottom-0" width="50px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ID</font></font></th>
                                <th class="wd-15p border-bottom-0" width="350px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Evento</font></font></th>
                                <th class="wd-15p border-bottom-0" width="350px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dirección</font></font></th>
                                <th class="wd-20p border-bottom-0" width="350px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fecha</font></font></th>
                                <th class="wd-15p border-bottom-0" width="50px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Acciones</font></font></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01</td>
                                <td>
                                    <span>Graduación UPV generación 2019..</span>
                                </td>
                                <td>
                                    <span>Av. Nuevas Tecnologías 5902, Parque Científico y Tecnológico de Tamaulipas, 87138 Cd Victoria, Tamps.</span>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <div class="mr-3 mt-0 mt-sm-1 d-block">
                                            <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Fecha:</span> 2023-03-17</h6>
                                            <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Hora:</span> 13:30</h6>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                        <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                        <button class="action-btns1" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"><i class="feather feather-trash-2 text-danger"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
	</div>
	<!-- End Row-->
@endsection