@extends('app')

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Configuración</h4>
    </div>
</div>
<!--End Page header-->

<!-- Row -->
<div class="row">
    {{-- Tarjeta submenu --}}
    {{-- <div class="col-xl-3 col-md-12 col-lg-12">
        <div class="card">
            <div class="table-responsive">
                <ul class="side-menu">
                    <li class="slide">
                        <a class="side-menu__item" href="#perfil">
                            <i class="feather  feather-user sidemenu_icon"></i>
                            <span class="side-menu" >Ajustes de cuenta</span>
                        </a>
                    </li>
                    <li class="slide">
                        <a class="side-menu__item" href="#pagos">
                            <i class="feather  feather-credit-card sidemenu_icon"></i>
                            <span class="side-menu">Modos de pago</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}

    {{-- Tarjeta Información usuario --}}
    <div class="col-xl-12 col-md-12 col-lg-12" id="perfil">
        <div class="card">
            <form method="POST" action="{{ route('user-profile-information.update') }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <h4 class="mb-5 font-weight-semibold">Información básica</h4>

                    {{-- Fila Nombre usuario y Correo --}}
                    <div class="row">
                        {{-- Nombre usuario --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-label mb-0 mt-2"> {{ __('Username') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name" placeholder="Ingresa un nombre de usuario" autofocus
                                       value="{{ old('name') ?? auth()->user()->name }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- Correo --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label mb-0 mt-2" for="email"> {{ __('E-Mail Address') }}</label>
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                       name="email" placeholder="Ingresa un correo electronico"
                                       value="{{ old('email') ?? auth()->user()->email }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    {{-- Button Guardar--}}
                    <div class="card-footer text-right">
						<button type="submit" class="btn btn-primary">
							<i class="feather  feather-save sidemenu_icon"></i>
							{{ __('Update Profile') }}
                        </button>
				    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Row-->


<!-- Row -->
{{-- <div class="row" id="pagos">
    <div class="col-xl-3 col-md-12 col-lg-12">
    </div>

    <div class="col-xl-9 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header d-xl-flex d-block">
                <div class="card-leftheader">
                    <h4 class="card-title" id="impuestos">Modos de pago</h4>
                </div>
                <div class="card-rightheader ml-md-auto">
                    <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
                        <div class="btn-list">
                            <a href="#" class="btn btn-outline-primary mr-3">
                                <i class="feather  feather-plus sidemenu_icon"></i>
                                Agregar modo de pago</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-vcenter text-nowrap border-top mb-0 invoice-table">
                            <thead>
                                <tr>
                                    <th class="wd-10p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nombre</font></font></th>
                                    <th class="wd-15p border-bottom-0" width="150px"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Acciones</font></font></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>Tarjeta de credito</span>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                            <button class="action-btns1" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"><i class="feather feather-trash-2 text-danger"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Efectivo</span>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                            <a href="#" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                            <button class="action-btns1" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"><i class="feather feather-trash-2 text-danger"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!-- End Row-->
@endsection

