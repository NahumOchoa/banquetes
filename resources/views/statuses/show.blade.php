@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver Estatus {{ $status->id }}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('status.index') }}">Estatus</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver Estatus</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-6 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-header d-xl-flex d-block">
                <h4 class="mb-5 font-weight-semibold">Información del Estatus</h4>
            </div>
			<div class="card-body">
                <div class="form-group">
                    <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>
                    <span class="mb-2 fs-14">{{ $status->name }}</span>
                </div>
			</div>
            <div class="card-footer text-right">
				<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
			</div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection
