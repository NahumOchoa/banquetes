@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Agregar Estatus</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('status.index') }}">Estatus</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Agregar Estatus</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
    <form action="{{ route('status.store') }}" method="POST" enctype="multipart/form-data">
	<div class="col-xl-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body">
                <h4 class="mb-5 font-weight-semibold">Información</h4>
                @include('statuses.partials.form', ['create' => true])
			</div>
            <div class="card-footer text-right">
                <a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
				<button type="submit" class="btn btn-primary">
					<i class="feather feather-save sidemenu_icon"></i>
					Guardar
				</button>
			</div>
		</div>
	</div>
    </form>
</div>
<!-- End Row-->
@endsection


@section('extra-script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
	<script src="{{ asset('js/confirmExit.js') }}"></script>
	<script src="{{ asset('js/characterCounter.js') }}"></script>
@endsection
