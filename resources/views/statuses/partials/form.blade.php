@csrf
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}*</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}@isset($status){{$status->name}}@endisset" required autocomplete="name" autofocus maxlength="25">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
