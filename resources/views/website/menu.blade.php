@extends('page')



@section('custom-css')
<style>
    body{
        background-image: url(" {{asset('images/bg-principal2.png')}} "), linear-gradient(rgb(245, 245, 220), rgb(245, 245, 220));
        background-position: center;
        background-repeat: no-repeat;
        /* background-size: cover; */
    }
    .cabecera{
        font-family: 'Oleo Script', cursive;
        color: #777259;
    }
    .img-responsive{
        margin-left: 100px;
    }
    .footers-info{
        margin-left: 20px;
    }
</style>
@endsection

@section('contenido')

        <div class="row pl-4">

            <div class="row">
                {{-- Tarjeta reservar --}}
                <div class="col-md-3" >
                    <div class="card p-6" style="background: #acb6e5;  /* fallback for old browsers */
                                                background: -webkit-linear-gradient(to right, #86fde8, #acb6e5);  /* Chrome 10-25, Safari 5.1-6 */
                                                background: linear-gradient(to right, #86fde8, #acb6e5); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */">
                        <img src="{{asset('images/buffet.png')}}" class="mb-6">
                        <span class="text-center h4 mb-8">Para reservar llama al <br> 834-444-4787 <br> o presiona el siguiente botón.</span>
                        <button id="button_reservar" class="btn btn-success" style="background: #56ab2f;
                            background: -webkit-linear-gradient(to right, #a8e063, #56ab2f);
                            background: linear-gradient(to right, #a8e063, #56ab2f); font-weight: bold;"
                            onclick="javascript:window.location = '{{route('website.reservar')}}'">
                            ¡Reserva ahora!</button>
                    </div>
                </div>

                {{-- Tarjeta derecha --}}
                <div class="col-md-9">
                    {{-- filtros --}}
                    <div class="row mb-3">

                        <div class="col-md-1 bg-white text-center mr-2" style="border-radius: 15px; background: #ff9966;
                            background: -webkit-linear-gradient(to right, #ff5e62, #ff9966);
                            background: linear-gradient(to right, #ff5e62, #ff9966); " role="button" onclick="javascript:window.location = '{{route('website.menu')}}'">
                            <div class="row mt-2">
                                <img class="d-block w-90 mx-auto rounded-4" src="{{asset('images/todo.png')}}" style="border-radius:10px;"> <br>
                            </div>
                            <div class="row mt-1">
                                <p style="color: #fff;">Todo</p>
                            </div>
                        </div>

                        @foreach ($categoriasMenu as $category)
                        <div class="col-md-1 bg-white text-center mr-2" style="border-radius: 15px; background: #ff9966;
                        background: -webkit-linear-gradient(to right, #ff5e62, #ff9966);
                        background: linear-gradient(to right, #ff5e62, #ff9966); " role="button" onclick="document.getElementById('category_id_{{$category->id}}').submit();">
                            <div class="row mt-2">
                                <img class="d-block w-90 mx-auto rounded-4" src="{{asset('images/todo.png')}}" style="border-radius:10px;"> <br>
                            </div>
                            <div class="row mt-1">
                                <p style="color: #fff;">{{$category->name}}</p>
                            </div>
                            <form action="{{route('website.menu.category')}}" method="post" id="category_id_{{$category->id}}">
                                @csrf
                                <input type="hidden" name="category_id" value="{{$category->id}}">
                            </form>
                        </div>
                        @endforeach
                        
                    </div>

                    <!-- Barra busquda -->
                    <div class="row mb-3" >
                        <form class="form-inline " action="{{route('website.menu.search')}}" method="POST" id="search" name="search">
                            @csrf
                            <input class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Search"
                              aria-label="Search" style="border-radius:10px;" id = "search_text" name = "search_text" @isset ($search_name) value = {{$search_name}} @endisset>
                            <i class="feather feather-search" aria-hidden="true" role="button" onclick="document.getElementById('search').submit();"></i>
                        </form >
                    </div>

                    {{-- platillos --}}
                    <!-- Platillos -->
                    @php
                        $i=1;    
                    @endphp
                    @foreach ($platillos as $platillo)
                        @if (($i % 3) == 1)
                            <div class="row mb-4">
                        @endif
                                <div class="col-md-3 bg-white pt-3 pb-3 shadow text-left mr-4" style="border-radius: 15px; background: #FFEFBA;
                                                        background: -webkit-linear-gradient(to right, #FFFFFF, #FFEFBA);
                                                        background: linear-gradient(to right, #FFFFFF, #FFEFBA); ">
                                    <div class="container">
                                        <img class="d-block w-100 mx-auto rounded-4" src="{{asset($platillo->url_img)}}" style="border-radius:10px;"> <br>
                                        <h4>{{$platillo->name}}</h4>
                                        <p>{{$platillo->description}}</p>
                                        <span style="font-weight: bold;">{{$platillo->price}}</span>
                                    </div>
                                </div>
                        @if (($i % 3) == 0)
                            </div>
                        @endif
                        @php
                            $i++;    
                        @endphp
                    @endforeach

                </div>
            </div>


        </div>

        {{-- Footer --}}

<footer>
<section class="footers border  pt-5 pb-3" style="background-color: #777259;">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 footers-one ">
            <div class="footers-logo">
                <img class="img-responsive" src="{{asset('images/logo.png')}}" style="width: 50px;">
            </div>

            <div class="footers-info mt-3" style="color: #ffffff">
                <p>Porque el sabor es lo que nos define</p>
            </div>

            <div class="social-icons">
                <a href="https://www.facebook.com/"><i id="social-fb" class="fab fa-facebook-square fa-2x social"></i></a>
                <a href="https://twitter.com/"><i id="social-tw" class="fab fa-twitter-square fa-2x social"></i></a>
                <a href="https://plus.google.com/"><i id="social-gp" class="fab fa-google-plus-square fa-2x social"></i></a>
                <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-2x social"></i></a>
            </div>
        </div>

    <div class="col-xs-12 col-sm-6 col-md-2 footers-three" style="color: #ffffff">
        <h5>Informacion </h5>
        <ul class="list-unstyled" >
            <li><a href="maintenance.html" style="color: #ffffff">Registrarse</a></li>
            <li><a href="about.html" style="color: #ffffff">Blog</a></li>
            <li><a href="about.html" style="color: #ffffff">Servicios</a></li>
        </ul>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-2 footers-five" style="color: #ffffff">
        <h5>Compañia</h5>
        <ul class="list-unstyled">
            <li><a href="about.html" style="color: #ffffff">Terminos</a></li>
            <li><a href="about.html" style="color: #ffffff">Politicas</a></li>
            <li><a href="contact.html" style="color: #ffffff">Contactanos</a></li>
        </ul>
    </div>
</section>


<section class="copyright border" style="background-color: #777259;" style="color: #ffffff">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12 pt-3" style="color: #ffffff">
                <p class="text-muted" >© 2022 Banquetes Cristy</p>
            </div>
        </div>
    </div>
</section>
</footer>


@endsection
