@extends('app')

@section('contenido')
                        
        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Agregar empleado</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="/empleados">Empleados</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Agregar empleados</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body">

				{{-- AGREGAR CONTENIDO AQUI --}}
				
			</div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection



@section('extra-script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
	<script src="{{ asset('js/confirmExit.js') }}"></script>
	<script src="{{ asset('js/characterCounter.js') }}"></script>
@endsection

