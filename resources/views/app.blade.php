<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>

		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta content="Banquetes Cristy" name="author">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Title -->
		<title>Banquetes</title>

        

        <!--Chartjs-->
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

		<!-- Bootstrap css -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

		<!-- Style css -->
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" />

		<!-- SwetAlert css -->
		<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">

		<!--Sidemenu css -->
        <link  href="{{ asset('css/sidemenu.css') }}" rel="stylesheet">

		<!---Icons css-->
		<link href="{{ asset('plugins/icons/icons.css') }}" rel="stylesheet" />

		<!-- P-scroll bar css: Desplazamiento en panel de navegación-->
		<link href="{{ asset('plugins/p-scrollbar/p-scrollbar.css') }}" rel="stylesheet" />

		<!-- Select2-->
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

		<!-- Bootstrap Toggle -->
		<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

        @yield('extra-css')

		<link id="deuteranopia" href="#" rel="stylesheet" />
	</head>

	<body class="app sidebar-mini" id="index1">

		

		<div class="page">
			<div class="page-main">

                <!--aside open-->
				<x-menu-vertical />
				<!--aside closed-->

				<div class="app-content main-content">
					<div class="side-app">

                    <!--app header-->
                    <x-navbar />
                    <!--/app header-->
                        @include('partials.alerts')
						<!--Row-->
						@yield('contenido')
				</div><!-- end app-content-->
			</div>
		</div>

		<!-- JQuery-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

		<!-- Bootstrap4 js-->
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

		<!-- SwetAlert js -->
		<script src="{{ asset('js/sweetalert2.min.js') }}"></script>

		<!--Ocultar/Mostrar panel de navegación-->
		<script src="{{ asset('plugins/sidemenu/sidemenu.js') }}"></script>


		<!-- P-scroll js: Desplazamiento en panel de navegación-->
		<script src="{{ asset('plugins/p-scrollbar/p-scrollbar.js') }}"></script>
		<script src="{{ asset('plugins/p-scrollbar/p-scroll1.js') }}"></script>

		<!-- Custom js-->
		<script src="{{ asset('js/custom.js') }}"></script>

		<!-- Select2-->
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

		<!-- Bootstrap Toggle -->
		<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


		<script>
			$(function() {
			  $('#toggle-event').change(function() {
				  if ($(this).prop('checked')) {
					document.getElementById('deuteranopia').href = "{{ asset('css/deuteranopia.css') }}";

				  } else {
					document.getElementById('deuteranopia').href = "#" ;
				  }
			  })
			})
		</script>
		@yield('extra-script')

	</body>
</html>
