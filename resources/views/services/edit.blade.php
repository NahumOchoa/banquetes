@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Editar Servicio {{ $service->id }}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('services.index') }}">Servicios</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Editar Servicio</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	{{--FORMULARIO--}}
    <form action="{{ route('services.update', $service->id) }}" method="POST" enctype="multipart/form-data">
	<div class="col-xl-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body">
                <h4 class="mb-5 font-weight-semibold">Información</h4>
                @method('PATCH')
				{{--SE INCLUYE EL FORMULARIO--}}
                @include('services.partials.form', ['edit'=> true])
			</div>
			{{--BOTONES--}}
            <div class="card-footer text-right">
                <a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
				<button type="submit" class="btn btn-primary">
					<i class="feather feather-save sidemenu_icon"></i>
					Guardar
				</button>
			</div>
		</div>
	</div>
    </form>
	{{--FIN FORMULARIO--}}
</div>
<!-- End Row-->
@endsection
