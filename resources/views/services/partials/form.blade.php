@csrf
<div class="row">
    <div class="col-sm-3">
        <!--left col-->
        <div class="text-center user-pic">
            <img src="https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png"
                 class="avatar-xxl rounded-cimb-1" alt="Foto servicio" name="image" id="image">
            <h6>Suba una imagen...</h6>
            <label class="mb-0">
                <span class="btn ripple btn-primary">
                    <i class="feather feather-upload sidemenu_icon"></i>
                    Examinar <input type="file" class="file-browserinput" style="display: none;" accept="image/*"
                                    name="url_image" id="url_image" onchange="previewImage(event)">
                </span>
            </label>
        </div>
    </div>
    <div class="col-sm-9">

        {{--NOMBRE--}}
        <div class="form-group">
            <label for="name" class="form-label">{{ __('Name') }}*</label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                   name="name" value="{{ old('name') }}@isset($service){{$service->name}}@endisset"
                   required autocomplete="name" autofocus placeholder="Ingresa el nombre del servicio" maxlength="50">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        {{--DESCRIPCION--}}
        <div class="form-group">
            <label for="description" class="form-label">Descripción</label>
            <input id="description" type="text" class="form-control @error('description') is-invalid @enderror"
                   name="description" value="{{ old('description') }}@isset($service){{$service->description}}@endisset"
                   required autocomplete="description" autofocus placeholder="Ingresa una breve descripción del servicio" maxlength="250">
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        {{--CANTIDAD--}}
        <div class="form-group">
            <label class="form-label" for="quantity">Cantidad</label>
            <input class="form-control @error('quantity') is-invalid @enderror" type="number" placeholder="1" min="1"
                   name="quantity" id="quantity" step="1" max="5" value="@isset($edit){{$service->quantity}}@endisset">
            @error('quantity')
                <span class="invalid-feedback" role="alert">
                    {{ $message }}
                </span>
            @enderror
        </div>
            
            {{--PRECIO--}}
        <div class="form-group">
            <label class="form-label" for="price">Precio</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input class="form-control @error('price') is-invalid @enderror" type="number" placeholder="0.00" min="0"
                       name="price" id="price" step="0.01" value="@isset($edit){{$service->price}}@endisset">
{{--                <span class="input-group-text">.00</span>--}}
                @error('price')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
        </div>
    </div>


</div>

<script type="text/javascript">
	const previewImage = e => {
		const preview = document.getElementById('image');
		preview.src = URL.createObjectURL(e.target.files[0]);
		preview.onload = () => URL.revokeObjectURL(preview.src);
	};
</script>
