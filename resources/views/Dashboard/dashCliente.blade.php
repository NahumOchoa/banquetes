@extends('app')

@section('contenido')
<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
         <h4 class="page-title">Hola
             @if(strlen(Auth::user()->profile->first_name) < 2)
                 {{Auth::user()->name}}
             @else
                 {{Auth::user()->profile->first_name}}
             @endif
         </h4>
         <span class="text-muted mb-1 fs-16">Estas son tus últimas reservaciones</span>
        {{-- <span class="text-muted mb-1 fs-16">Hola <span class="mb-1 fs-16 fw-bold">Juan Carlos</span>, estas son tus ultimas reservaciones</span> --}}
    </div>
</div>
<!--End Page header-->




<!-- Row-->
<div class="row">
    {{-- ================ T A R J E T A   R E S E R V A C I O N E S   R E C I E N T E S ================ --}}
    <div class="col-xl-12 col-md-12">
        <div class="card overflow-hidden">
            <div class="card-header border-0">
                <h4 class="card-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reservaciones recientes</font></font></h4>
            </div>
            <div class="card-body p-0 pt-4">
                <div class="table-responsive">
                    <table class="table table-vcenter text-nowrap border-top mb-0 invoice-table">
                        <thead>
                            <tr>
                                <th class="wd-10p border-bottom-0" width='10%'><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ID</font></font></th>
                                <th class="wd-10p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Evento</font></font></th>
                                <th class="wd-15p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dirección</font></font></th>
                                <th class="wd-20p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fecha</font></font></th>
                                <th class="wd-15p border-bottom-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Acciones</font></font></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bookings as $booking)
                            <tr>
                                <td>{{$booking->id}}</td>
                                <td>
                                    <span>{{$booking->name}}</span>
                                </td>
                                <td>
                                    <span>{{$booking->address->street_name}}. México, {{$booking->address->zip_code}} <br> Cd Victoria, Tamaulipas</span>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <div class="mr-3 mt-0 mt-sm-1 d-block">
                                            <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Fecha:</span> {{$booking->booking_date}}</h6>
                                            <h6 class="mb-1 fs-14"><span class="text-muted mb-0 fs-14">Hora:</span> {{$booking->booking_time}}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ route('reservaciones.show', $booking->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- ================ T A R J E T A   A C T I V I D A D   R E C I E N T E ================ --}}
    {{-- <div class="col-xl-4 col-md-12">
        <div class="card">
            <div class="card-header border-bottom-0">
                <h4 class="card-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Actividad reciente</font></font></h4>
            </div>
            <div class="card-body">
                <div class="mb-5">
                    <div class="list-group-item d-flex p-0  align-items-center border-0">
                        <div class="d-flex">
                            <div class="mt-1">
                                <h6 class="mb-1 font-weight-semibold fs-16"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mario Moreno Cantinflas
                                     </font></font><span class="font-weight-normal text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">solicita un cambio en </font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Graduación UPV
                                </font></font></h6>
                                <span class="clearfix"></span>
                                <span class="fs-14 text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hace 30 minutos</font></font></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-5">
                    <div class="list-group-item d-flex p-0  align-items-center border-0">
                        <div class="d-flex">
                            <div class="mt-1">
                                <h6 class="mb-1 font-weight-semibold fs-16"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Forrest Gump
                                     </font></font><span class="font-weight-normal text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">solicita cancelar </font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Fiesta teniente Dan
                                </font></font></h6>
                                <span class="clearfix"></span>
                                <span class="fs-14 text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hace 45 minutos</font></font></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-5">
                    <div class="list-group-item d-flex p-0  align-items-center border-0">
                        <div class="d-flex">
                            <div class="mt-1">
                                <h6 class="mb-1 font-weight-semibold fs-16"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Forrest Gump
                                     </font></font><span class="font-weight-normal text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">solicita cancelar </font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Fiesta teniente Dan
                                </font></font></h6>
                                <span class="clearfix"></span>
                                <span class="fs-14 text-muted"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hace 45 minutos</font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>

@endsection
