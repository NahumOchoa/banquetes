<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>

		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta content="Banquetes" name="author">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Title -->
		<title>Banquetes </title>

   
		<!-- Bootstrap css -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

		<!-- Style css -->
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" />

		<!-- SwetAlert css -->
		<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">

		<!--Sidemenu css -->
        <link  href="{{ asset('css/sidemenu.css') }}" rel="stylesheet">

		<!---Icons css-->
		<link href="{{ asset('plugins/icons/icons.css') }}" rel="stylesheet" />

		<!-- P-scroll bar css: Desplazamiento en panel de navegación-->
		<link href="{{ asset('plugins/p-scrollbar/p-scrollbar.css') }}" rel="stylesheet" />

        {{-- Fuente para titulo --}}
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Oleo+Script&display=swap" rel="stylesheet">

        <!-- Select2-->
		<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        @yield('custom-css')

        {{-- <link href="{{ asset('css/deuteranopia.css') }}" rel="stylesheet" /> --}}

	</head>

	<body class="app sidebar-mini" id="index1">

		<div class="page">
			<div class="page-main">
                <!--NavBar-->
                <div class="container mb-9">
                    <nav class="navbar fixed-top navbar-light bg-white navbar-expand-md bg-faded justify-content-center mb-5">
                    
                        
                       
                    </nav>
                </div>
                <!--/NavBar-->


                <!-- Contenido -->
                @yield('contenido')
		        <!-- End Contenido-->




			</div>
		</div>

		<!-- JQuery-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

		<!-- Bootstrap4 js-->
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

		<!-- SwetAlert js -->
		<script src="{{ asset('js/sweetalert2.min.js') }}"></script>

		<!--Ocultar/Mostrar panel de navegación-->
		<script src="{{ asset('plugins/sidemenu/sidemenu.js') }}"></script>


		<!-- P-scroll js: Desplazamiento en panel de navegación-->
		<script src="{{ asset('plugins/p-scrollbar/p-scrollbar.js') }}"></script>
		<script src="{{ asset('plugins/p-scrollbar/p-scroll1.js') }}"></script>

		<!-- Custom js-->
		<script src="{{ asset('js/custom.js') }}"></script>

        <!-- Select2-->
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        @yield('extra-script')

	</body>
</html>
