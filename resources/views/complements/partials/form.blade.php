@csrf
<div class="row">
    <div class="col-sm-3">
        <!--left col-->
        <div class="text-center user-pic">
            @isset($edit)
                <img src="{{asset($complement->url_image)}}"
                 class="avatar-xxl rounded-cimb-1" alt="Foto servicio" name="image" id="image">
            @endisset
            @isset($create)
                <img src="https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png"
                 class="avatar-xxl rounded-cimb-1" alt="Foto servicio" name="image" id="image">
            @endisset

            <h6>Suba una imagen...</h6>
            <label class="mb-0">
                <span class="btn ripple btn-primary">
                    <i class="feather feather-upload sidemenu_icon"></i>
                    Examinar <input type="file" class="file-browserinput" style="display: none;" accept="image/*"
                                    name="url_image" id="url_image" onchange="previewImage(event)">
                </span>
            </label>
        </div>
    </div>
    <div class="col-sm-9">

        {{--NOMBRE--}}
        <div class="form-group">
            <label for="name" class="form-label">{{ __('Name') }}*</label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                   name="name" value="{{ old('name') }}@isset($complement){{$complement->name}}@endisset"
                   required autocomplete="name" autofocus placeholder="Ingresa el nombre del complemento" maxlength="255">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

    </div>


</div>

<script type="text/javascript">
	const previewImage = e => {
		const preview = document.getElementById('image');
		preview.src = URL.createObjectURL(e.target.files[0]);
		preview.onload = () => URL.revokeObjectURL(preview.src);
	};
</script>
