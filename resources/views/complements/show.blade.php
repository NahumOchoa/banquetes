@extends('app')

@section('contenido')

        <!--Page header-->
	<div class="page-header d-xl-flex d-block">
		<div class="page-leftheader">
			<h4 class="page-title">Ver Complemento {{ $complement->id }}</h4>
			<ul class="breadcrumb">
				<li class="mb-1 fs-16"><a href="{{ route('complements.index') }}">Complementos</a></li>
				<li class="text-muted mb-1 fs-16 ml-2 mr-2"> / </li>
				<li class="text-muted mb-1 fs-16">Ver Complemento</li>
			</ul>
		</div>

	</div>
	<!--End Page header-->

<!-- Row -->
<div class="row">
	<div class="col-xl-6 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-header d-xl-flex d-block">
                <h4 class="mb-5 font-weight-semibold">Información del Complemento</h4>
            </div>
			<div class="card-body">
                <div class="user-pic text-center">
					@if(is_null($complement->url_image))
					    <img src="https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png"
                             alt="user-img" class="avatar-xxl rounded-circle mb-1" alt="Foto producto">
					@else
					    <img src="{{ asset($complement->url_image) }}" alt="user-img" class="avatar-xxl rounded-circle mb-1"
                             alt="Foto servicio">
					@endif
				</div>
                <div class="form-group">
                    <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>
                    <span class="mb-2 fs-14">{{ $complement->name }}</span>
                </div>
			</div>
            <div class="card-footer text-right">
				<a role="button" class="btn btn-outline-dark" href="{{ url()->previous() }}">
					<i class="feather feather-corner-down-left sidemenu_icon"></i>
					Regresar
				</a>
			</div>
		</div>
	</div>
</div>
<!-- End Row-->
@endsection
