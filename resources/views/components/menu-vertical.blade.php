<!--aside open-->
<aside class="app-sidebar">

	<div class="app-sidebar3">

		<div class="app-sidebar__user">
			<div class="dropdown user-pro-body text-center">
				<div class="user-pic">
                    @if(is_null(Auth::user()->profile->url_image))
                        <img src="{{ asset('images/usuario.png') }}" alt="img" class="avatar-xxl rounded-circle mb-1">
                    @else
                        <img src="{{ asset(Auth::user()->profile->url_image) }}" alt="img" class="avatar-xxl rounded-circle mb-1">
                    @endif
				</div>
				<div class="user-info">
					<h5 class=" mb-2">
                        @if(strlen(Auth::user()->profile->first_name.' '.Auth::user()->profile->last_name) < 2)
                            {{Auth::user()->name}}
                        @else
                            {{Auth::user()->profile->first_name.' '.Auth::user()->profile->last_name}}
                        @endif
                    </h5>
					<span class="text-muted app-sidebar__user-name text-sm">{{Auth::user()->roles[0]->name}}</span>
				</div>
			</div>
		</div>

		<ul class="side-menu">
			<li class="side-item side-item-category mt-4">Tablero</li>
			<li class="slide">
					<a class="side-menu__item {{ (Request::is('dashboard') or Request::is('dashboard/customer')) ? 'active' : '' }}" data-toggle="slide" href="/" id="dashboard">
					<i class="feather feather-home sidemenu_icon"></i>
					<span class="side-menu__label">Tablero</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
			@can('is-admin')
             <li class="slide">
				<a class="side-menu__item {{ Request::is('admin/users') ? 'active' : '' }}" data-toggle="slide" href="{{ route('admin.users.index') }}" id="usuarios">
					<i class="feather feather-user sidemenu_icon"></i>
					<span class="side-menu__label">Usuarios</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
            @endcan
{{--            <li class="slide">--}}
{{--				<a class="side-menu__item {{ Request::is('clientes') ? 'active' : '' }}" data-toggle="slide" href="/clientes" id="clientes">--}}
{{--					<i class="feather feather-user sidemenu_icon"></i>--}}
{{--					<span class="side-menu__label">Clientes</span><i class="angle fa fa-angle-right"></i>--}}
{{--				</a>--}}
{{--			</li>--}}

{{--            <li class="slide">--}}
{{--				<a class="side-menu__item {{ Request::is('empleados') ? 'active' : '' }}" data-toggle="slide" href="/empleados" id="empleados">--}}
{{--					<i class="feather feather-users sidemenu_icon"></i>--}}
{{--					<span class="side-menu__label">Empleados</span><i class="angle fa fa-angle-right"></i>--}}
{{--				</a>--}}
{{--			</li>--}}
            @can('is-admin', 'is-employee')
            <li class="slide">
				<a class="side-menu__item {{ Request::is('menus') ? 'active' : '' }}" data-toggle="slide" href="{{ route('menus.index') }}" id="menu">
					<svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.7" stroke-linecap="round" stroke-linejoin="round" class="feather feather-coffee" style="margin-right: 7px;">
						<path d="M18 8h1a4 4 0 0 1 0 8h-1"></path>
						<path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path>
						<line x1="6" y1="1" x2="6" y2="4"></line>
						<line x1="10" y1="1" x2="10" y2="4"></line>
						<line x1="14" y1="1" x2="14" y2="4"></line>
					</svg>
					<span class="side-menu__label">Menú</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
            @endcan
            @can('is-admin', 'is-employee')
            <li class="slide">
				<a class="side-menu__item {{ Request::is('complements') ? 'active' : '' }}" data-toggle="slide" href="{{ route('complements.index') }}" id="menu">
					<i class="feather feather-plus-square sidemenu_icon"></i>
					<span class="side-menu__label">Complementos</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
            @endcan
            @can('is-admin', 'is-employee')
			<li class="slide">
				<a class="side-menu__item  {{ Request::is('reservaciones') ? 'active' : '' }}" data-toggle="slide" href="/reservaciones" id="reservaciones">
					<i class="feather feather-calendar sidemenu_icon"></i>
					<span class="side-menu__label">Reservaciones</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
            @endcan
            @can('is-admin', 'is-employee')
            <li class="slide">
				<a class="side-menu__item {{ Request::is('services') ? 'active' : ''}}" data-toggle="slide" href="{{ route('services.index') }}" id="service">
					<i class="feather feather-info sidemenu_icon"></i>
					<span class="side-menu__label">Servicios</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
            @endcan
        
           

			<li class="slide">
				<a class="side-menu__item {{ Request::is('profile') ? 'active' : '' }}" data-toggle="slide" href="{{ route('profile.index') }}" id="configuraciones">
					<i class="feather  feather-settings sidemenu_icon"></i>
					<span class="side-menu__label">Configuraciones</span><i class="angle fa fa-angle-right"></i>
				</a>
			</li>
		</ul>

           

      
	</div>
</aside>
<!--aside closed-->
