@extends('app')

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
@endsection

@section('contenido')

<!--Page header-->
<div class="page-header d-xl-flex d-block">
    <div class="page-leftheader">
        <h4 class="page-title">Menu</h4>
    </div>
    <div class="page-rightheader ml-md-auto">
        <div class="align-items-end flex-wrap my-auto right-content breadcrumb-right">
            <div class="btn-list">
                <a href="{{ route('menus.create') }}" class="btn btn-primary mr-3">
                    <i class="feather feather-plus sidemenu_icon"></i>
                    Agregar Platillo</a>
            </div>
        </div>
    </div>
</div>
<!--End Page header-->


{{-- ////////////// S O L O   C A M B I A R   L O S   V A L O R E S   N E C E S A R I O S ////////////// --}}

<!-- Row -->
<div class="row">
    @include('sweetalert::alert')
    <div class="col-xl-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header  border-0">
                <h4 class="card-title">Lista de Platillos</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    {{--DATATABLE--}}
                    <table class="table table-vcenter text-wrap table-bordered border-bottom" id="services-list">
                        <thead>
                            <tr>
                                <th class="border-bottom-0">ID</th>
                                <th class="border-bottom-0">{{ __('Name') }}</th>
                                <th class="border-bottom-0">Descripción</th>
                                <th class="border-bottom-0">Precio</th>
                                <th class="border-bottom-0">Categoria</th>
                                <th class="border-bottom-0">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($menus)
                                @foreach($menus as $menu)
                                    <tr>
                                        <td> {{ $menu->id }}</td>
                                        <td>
                                            <div class="d-flex">
                                                @if (is_null($menu->url_img))
                                                    <span class="avatar avatar-md brround mr-3" style="border-color: transparent; background-color:transparent; background-image: url({{'https://cdn2.iconfinder.com/data/icons/food-drinks-vol-3/48/105-512.png'}}"></span>
                                                @else
                                                    <span class="avatar avatar-md brround mr-3" style="border-color: transparent; background-color:transparent; background-image: url({{asset($menu->url_img)}}"></span>
                                                @endif
                                                <div class="mt-0 mt-sm-2 d-block">
                                                    <h6 class="mb-1 fs-17">{{ $menu->name }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td> {{ $menu->description}}</td>
                                        <td style="text-align: right"> ${{ number_format($menu->price, 2) }}</td>
                                        <td style="text-align: right"><span class="badge rounded-pill bg-primary">{{ $menu->category->name}}</span></td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('menus.show', $menu->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Ver"><i class="feather feather-eye text-primary"></i></a>
                                                <a href="{{ route('menus.edit', $menu->id) }}" class="action-btns1" data-toggle="tooltip" data-placement="top" title="Editar"><i class="feather feather-edit-2  text-success"></i></a>
                                                <form action="{{ route('menus.destroy', $menu->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="action-btns1 feather feather-trash-2 text-danger show_confirm" data-toggle="tooltip" data-placement="top" title="Eliminar" type="submit"></button>
                                                </form>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                    {{--FIN DATATABLE--}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection
@section('extra-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready( function () {
             $("#services-list").DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/es-mx.json"
                }
            });
        });
        $('.show_confirm').click(function(event) {
            let form = $(this).closest("form");
            event.preventDefault();
            swal({
                title: "¿Esta seguro de eliminar este servicio?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Eliminar"],
            })
            .then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
