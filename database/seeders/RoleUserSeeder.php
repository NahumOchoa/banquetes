<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();
        $listAdmins = [
            'LeonardoR',
        ];
        User::all()->each(function ($user) use ($roles, $listAdmins){
            if (in_array($user->name, $listAdmins)){
                $user->roles()->attach(
                    $roles->only([1,3])->pluck('id')
                );
            }
            else{
                $user->roles()->attach(
                    $roles->except([1])->random(1)->pluck('id')
                );
            }
        });
    }
}
