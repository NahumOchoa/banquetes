<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'name'=> 'Rollo de carne envuelto en hojaldre con gravi',
            'description'=> 'Rollo de carne envuelto en hojaldre cubierto de gravy. Pure. Espaguetti al gusto. Bolillo rebanado.',
            'url_img'=> 'images/menu/menu_1.png',
            'price'=> 45,
            'category_id'=> 2,
        ]);
        DB::table('menus')->insert([
            'name'=> 'Pechuga de pollo a la Gordon Blue',
            'description'=> 'Pechuga de pollo a la Gordon Blue (Rellena de jamón y queso). Puré. Espagueti al gusto',
            'url_img'=> 'images/menu/menu_2.png',
            'price'=> 65,
            'category_id'=> 2,
        ]);
        DB::table('menus')->insert([
            'name'=> 'Pastel de carne con gravy',
            'description'=> 'Pastel de carne con gravy. Pure. Espagueti al gusto. Bolillo Rebanado.',
            'url_img'=> 'images/menu/menu_3.png',
            'price'=> 55,
            'category_id'=> 1,
        ]);
        DB::table('menus')->insert([
            'name'=> 'Pechuga de pollo almendrada',
            'description'=> 'Pechuga de pollo almendrada. Puré Espagueti al gusto. Bolillo rebanado.',
            'url_img'=> 'images/menu/menu_4.png',
            'price'=> 65,
            'category_id'=> 1,
        ]);
        DB::table('menus')->insert([
            'name'=> 'Pierna o muslo de pollo en achiote',
            'description'=> 'Pierna o muslo de pollo en achiote. Puré Espagueti al gusto Bolillo rebanado',
            'url_img'=> 'images/menu/menu_5.png',
            'price'=> 40,
            'category_id'=> 2,
        ]);
        DB::table('menus')->insert([
            'name'=> 'Pechuga de pollo en champiñones',
            'description'=> 'Pechuga de pollo en champiñones Puré Espagueti al gusto Bolillo rebanado',
            'url_img'=> 'images/menu/menu_6.png',
            'price'=> 65,
            'category_id'=> 1,
        ]);
    }
}
