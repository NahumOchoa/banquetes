<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Sequence;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::factory()->count(10)->create(
            new Sequence(
                ['user_id' => '8'],
                ['user_id' => '9'],
                ['user_id' => '10'],
                ['user_id' => '11'],
                ['user_id' => '12'],
                ['user_id' => '13'],
                ['user_id' => '14'],
                ['user_id' => '15'],
                ['user_id' => '16'],
                ['user_id' => '10']
            )
        );
    }
}
