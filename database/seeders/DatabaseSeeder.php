<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InventorySeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(AddressesSeeder::class);
        $this->call(ComplementSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(BookingSeeder::class);
        $this->call(BookingDetailSeeder::class);
        $this->call(ServiceSeeder::class);
    }
}
