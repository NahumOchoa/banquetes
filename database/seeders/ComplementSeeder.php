<?php

namespace Database\Seeders;

use App\Models\Complement;
use Illuminate\Database\Seeder;

class ComplementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Complement::factory(5)->create();
    }
}
