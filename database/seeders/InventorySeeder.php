<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventories')->insert([
            'name' => 'Plato',
            'quantity' => '10',
            'description' => 'Plato Grande',
            'url_image' => 'images/sample1.jpeg',
            'created_at' => now(),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Papas',
            'quantity' => '10',
            'description' => 'Papas',
            'url_image' => 'images/sample2.jpeg',
            'created_at' => now(),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pollo',
            'quantity' => '10',
            'description' => 'Pollo del 17',
            'url_image' => 'images/sample3.jpeg',
            'created_at' => now(),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Pescado',
            'quantity' => '10',
            'description' => 'Mojarra Fresca',
            'url_image' => 'images/sample4.jpeg',
            'created_at' => now(),
        ]);
        DB::table('inventories')->insert([
            'name' => 'Cubiertos',
            'quantity' => '10',
            'description' => 'Cubiertos de metal',
            'url_image' => 'images/sample5.jpeg',
            'created_at' => now(),
        ]);
    }
}
