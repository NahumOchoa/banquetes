<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Sequence;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(7)->create(
            new Sequence(
                [
                    'name' => 'LeonardoR',
                    'email' => '1930379@upv.edu.mx'
                ],
            )
        );
        Profile::factory()->count(7)->create(
            new Sequence(
                [
                    'first_name' => 'Leonardo',
                    'last_name' => 'Ramos',
                    'user_id' => '7',
                ],
            )
        );
        Profile::factory()->count(10)->create();
    }
}
