<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'name' => 'Mesero',
            'price' => 350,
            'description' => 'Mesero por 5 horas',
            'quantity' => 1,            
            'url_img' => 'images/services/mesero.png',
            'created_at' => now(),
        ]);
        DB::table('services')->insert([
            'name' => 'Loza',
            'price' => 6,
            'description' => 'Plato y tenedor',
            'quantity' => 1,            
            'url_img' => 'images/services/plato.png',
            'created_at' => now(),
        ]);
        DB::table('services')->insert([
            'name' => 'Vaso',
            'price' => 2,
            'description' => '',
            'quantity' => 1,            
            'url_img' => 'images/services/vaso.png',
            'created_at' => now(),
        ]);
        DB::table('services')->insert([
            'name' => 'Cuchillo',
            'price' => 2,
            'description' => '',
            'quantity' => 1,            
            'url_img' => 'images/services/cuchillo.png',
            'created_at' => now(),
        ]);
        DB::table('services')->insert([
            'name' => 'Cuernitos',
            'price' => 5,
            'description' => '',
            'quantity' => 1,            
            'url_img' => 'images/services/cuernitos.jpeg',
            'created_at' => now(),
        ]);
    }
}
