<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'phone_number' => $this->faker->regexify('834[0-9]{7}'),
            'gender' => 'Masculino',
            'url_img' => 'images/usuario.png',
            'user_id' => User::factory()->count(1)->create()->first(),
        ];
    }
}
