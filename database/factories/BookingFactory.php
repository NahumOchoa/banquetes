<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Complement;
use App\Models\Menu;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $menus = Menu::get(['id']);
        $complements = Complement::get(['id']);
        return [
            'menu_id' => $this->faker->randomElement($menus),
            'complement1_id' => $this->faker->randomElement($complements),
            'complement2_id' => $this->faker->randomElement($complements)
        ];
    }
}
