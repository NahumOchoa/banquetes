<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $bookings = Booking::get(['id']);
        $users = User::get(['id']);
        $addresses = Address::get(['id']);

        return [
            'name'          =>  $this->faker->word(),
            'quantity'      =>  $this->faker->numberBetween(100, 500),
            'description'   =>  $this->faker->text(75),
            'booking_date'  =>  $this->faker->date(),
            'booking_time'  =>  $this->faker->time(),
            'user_id'       =>  $this->faker->randomElement($users),
            'address_id'    =>  $this->faker->randomElement($addresses),
            'subtotal'      =>  $this->faker->randomFloat(2, 10000, 100000000),
            'total'         =>  $this->faker->randomFloat(2, 10000, 100000000),
            'status_id'     =>  1,
            'booking_id'    =>  $this->faker->randomElement($bookings),
        ];
    }
}
