<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'street_name'=>$this->faker->streetName(),
            'zip_code'=>12345,
            'description'=>$this->faker->text(),
            'city_id'=>1,
        ];
    }
}
