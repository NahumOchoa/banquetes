<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'description'=>$this->faker->paragraph,
            'url_img'=>'images/sample5.jpeg',
            'price'=>$this->faker->randomDigit,
            'category_id'=>$this->faker->randomElement([1,2,3]),
        ];
    }
}
