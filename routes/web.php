<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Employee\ComplementController;
use App\Http\Controllers\Employee\InventoryController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\Employee\PackageController;
use App\Http\Controllers\Employee\ServiceController;
use App\Http\Controllers\Employee\StatusController;
use App\Http\Controllers\Employee\MenuController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Website\MenuController as WebsiteMenuController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('dashboard.dashAdmin', [DashboardController::class, 'dashboardAdmin']);
// });



Route::get('/reservar', [BookingController::class, 'create'])->name('website.reservar');
Route::post('/reservar', [BookingController::class, 'store'])->name('reservar.store');

Route::get('/factura', function () {
    return view('facturas.plantilla-pdf');
});

Route::get('/inicio', function () {
    return view('website/principal');
})->name('website');

#Visualizar vista verificar correo
#Route::get('/verify', function () {
    #return view('auth/verify');
#})->name('website');
#

Route::controller(MenuController::class)->group(function () {
    Route::match(array('GET', 'POST'), 'website/menu', [WebsiteMenuController::class,'index'])->name('website.menu');
    Route::match(array('GET', 'POST'), 'website/menu/search', [WebsiteMenuController::class,'search'])->name('website.menu.search');
    Route::match(array('GET', 'POST'), 'website/menu/category', [WebsiteMenuController::class,'searchByCategory'])->name('website.menu.category');
});


//Route::get('menu', function () {
//    return view('menu.index');
//});
//Route::get('menu/crear', function () {
//    return view('menu.crear');
//});
//Route::get('menu/ver', function () {
//    return view('menu.ver');
//});



//Route::resource('inventario', InventoryController::class);

//Route::get('inventario/{id}', [InventoryController::class, 'show']);



//Route::get('configuracion', function () {
//    return view('configuracion.index');
//});

// Route::get('create-pdf-file', [PDFController::class, 'index']);

//Authenticated Routes
Route::middleware(['auth'])->group( function () {
    Route::get('/profile/edit', 'App\Http\Controllers\ProfileController@edit')->name('profile.edit');
    Route::get('/profile/change-password', 'App\Http\Controllers\ProfileController@change_password')->name('profile.change-password');
    Route::get('/profile', 'App\Http\Controllers\ProfileController@index')->name('profile.index');
    Route::patch('/profile', 'App\Http\Controllers\ProfileController@update')->name('profile.update');
});

//Admin Routes
Route::prefix('admin')->name('admin.')->middleware(['auth', 'auth.isAdmin'])->group( function () {
    Route::resource('/users', UserController::class);
});

//Employee routes
Route::middleware(['auth', 'verified', 'auth.isEmployee'])->group( function () {
    //TODO: Empleados
    Route::get('/empleados', function () {
        return view('empleados.index');
    });
    Route::get('/empleados/crear', function () {
        return view('empleados.crear');
    });
    //TODO: Clientes
    Route::get('/clientes', function () {
        return view('clientes.index');
    });
    Route::get('/clientes/crear', function () {
        return view('clientes.crear');
    });
    Route::get('/clientes/ver', function () {
        return view('clientes.ver');
    });
    Route::get('/reportes', function () {
        return view('reportes.index');
    });
    Route::get('/updateStatus', [BookingController::class, 'updateStatus'])->name('update-status');

    Route::get('/dashboard', [DashboardController::class, 'dashboardAdmin']);

    Route::post('create-pdf-file', [PDFController::class, 'index']);

    Route::resource('/reservaciones', BookingController::class);
    Route::resource('/inventario', InventoryController::class);
    Route::resource('/status', StatusController::class);
    Route::resource('/services',ServiceController::class);
    Route::resource('/menus', MenuController::class);
    Route::resource('/packages', PackageController::class);
    Route::resource('/complements',ComplementController::class);
});

//Customer routes
Route::middleware(['auth', 'auth.isCustomer'])->group( function () {
    Route::get('/dashboard/customer', [DashboardController::class, 'dashboardCliente'])->name('dashboardCustomer');
});

//Default routes

Route::get('/', [HomeController::class, 'index']);
