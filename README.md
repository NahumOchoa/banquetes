## Instalación del proyecto

1. Instalar dependencias:
	~~~
    composer install
    ~~~

2. Generar archivo de configuración
	En la raiz de la carpeta renombrar el archivo **.env.example** a **.env** o usar 
    ~~~
    cp .env.example .env
    ~~~

3. Generar API Key
    ~~~
	php artisan key:generate
    ~~~

4. Migraciones.
  * Crean una base de datos usando phpmyadmin o la consola de mysql.
  * Abren el archivo .env del punto 3.
  * Se colocan en el apartado de MySQL (linea 11 aprox)
  * Ahi editan el valor del campo DB_DATABASE, colocando el nombre de la base de datos que crearon. Quedando DB_DATABASE=nombreBaseDatos.
  * Despues solo ejecutan las migraciones con
    ~~~
    php artisan migrate --seed
    ~~~
    Genera las migraciones y llena la DB con datos predeterminados
 

## Extra
De preferencia crear un virtualhost para que no haya problemas con las rutas.

* Para Ubuntu:
    https://simplecodetips.wordpress.com/2018/07/11/crear-virtualhost-con-xampp-en-ubuntu-18-04/
    
    Otra opción es correr el comando: 
    ~~~
    php artisan serve
    ~~~
* Para Windows: 
    https://desarrolloweb.com/articulos/configurar-virtual-hosts-apache-windows.html

[Link](https://getcomposer.org/download/) para instalar composer

[Link](https://nodejs.org/es/) para instalar nodejs

[Link](https://laravel.com/docs/8.x/readme) Documentación de laravel

Todos los usuarios de prueba tienen la contraseña "password"
