	/*
		Obtiene el costo total de la reservación
	*/
	function obtenerTotal(){
		let montos	= document.getElementsByClassName('monto');
		let total	= document.getElementById('estimadoTotal');
		var suma	= 0;


		for (let i = 0; i < montos.length; i++) {
			if(montos[i].innerText != '' || !isNaN(montos[i].innerText) || montos[i].innerText != ' '){
				suma += moneyToNumber(montos[i].innerText);
			}
		}
		total.setAttribute('value', numberToMoney(suma));
	}