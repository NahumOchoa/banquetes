<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complement extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the complement 1 that owns the Complement.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complement_one()
    {
        return $this->hasMany('App\Models\Booking', 'complement1_id', 'id');
    }

    /**
     * Get the complement 2 that owns the Complement.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complement_two()
    {
        return $this->hasMany('App\Models\Booking', 'complement2_id', 'id');
    }

}
