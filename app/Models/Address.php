<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_name',
        'zip_code',
        'description',
        'city_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the profile that own the Address
     */
    public function profile(){
        $this->hasOne('App\Models\Profile');
        // $this->belongsTo('App\Models\Profile');
    }

    /**
     * Get the booking that own the Address
     */
    public function booking(){
        $this->hasOne('App\Models\Profile');
    }
}
