<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_id',
        'complement1_id',
        'complement2_id',
    ];

    /**
     * Get the menu that owns the Booking.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }

    /**
     * Get the package that owns the Booking.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }

    /**
     * Get the complement_1 that owns the Booking.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function complement_one()
    {
        return $this->belongsTo('App\Models\Complement', 'complement1_id', 'id', 'complements');
    }

    /**
     * Get the complement_2 that owns the Booking.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function complement_two()
    {
        return $this->belongsTo('App\Models\Complement', 'complement2_id', 'id', 'complements');
    }

    /**
     * Get the booking_detail that owns the Booking
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function booking_detail()
    {
        return $this->hasOne('App\Models\BookingDetail');
    }

    /**
     * Get the address that own the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    /**
     * Get the services that own the Booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany('App\Models\Service')->withPivot('quantity');
    }
}
