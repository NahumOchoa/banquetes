<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use RealRashid\SweetAlert\Facades\Alert;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        $this->reportable(function (Throwable $e) {

        });

        $this->renderable(function (\Illuminate\Database\QueryException $e, $request) {
            Alert::error('Error', 'No se pudo conectar con la base de datos');
            return redirect()->back();
        });

        $this->renderable(function (\Swift_TransportException $e, $request) {
            //para cuando no este configurado el mail, no es un error que el usuario se suponga que deba ver
            //Alert::error('Error', 'No se pudo conectar con la base de datos');
            return redirect()->route('admin.users.index');
        });



    }
}
