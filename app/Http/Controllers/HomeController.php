<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    public function index()
    {
        if (Gate::allows('is-admin') or Gate::allows('is-employee')){
            return redirect('dashboard');
        }
        if (Gate::allows('is-customer')){
            return redirect('dashboard/customer');
        }
        return redirect()->route('login');
    }
}
