<?php

namespace App\Http\Controllers;

use App\Models\BookingDetail;
use App\Models\User;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function dashboardAdmin(){

        // Code snippet taken from: https://es.stackoverflow.com/a/342336
        $clientes = count(User::whereHas('roles', function ($query) {
            return $query->where('name', 'Cliente');
        })->get());

        $reservacionesPendientes = count(BookingDetail::where('status_id', '1')->get());
        $reservacionesDelMes = BookingDetail::whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->get('total');
        $ultimasReservaciones = BookingDetail::latest()->take(5)->get();

        $ultimosPagos = BookingDetail::latest()->where('status_id', '2')->take(3)->get();


        $ganancias = 0;
        foreach ($reservacionesDelMes as $reservacion) {
            $ganancias += $reservacion->total;
        }

        $gananciasAnual = $this->gananciasAnual();

        return view('dashboard.dashAdmin', get_defined_vars());


    }



    public function dashboardCliente(){

        // Code snippet taken from: https://es.stackoverflow.com/a/342336
        $bookings = BookingDetail::where('user_id', auth()->user()->id)->get();

        return view('dashboard.dashCliente', get_defined_vars());


    }



    public function getGananciasMes($month_start, $month_end){

        $pagos = BookingDetail::select(['id', 'updated_at', 'total'])
        ->where('updated_at','>=',$month_start)
        ->Where('updated_at','<=',$month_end)
        ->where('status_id', 2)
        ->get();

        $ganancias_mes = 0;

        foreach ($pagos as $pago) {
            $ganancias_mes += $pago->total;
        }

        return (int)$ganancias_mes;
    }


    public function gananciasAnual(){
        $meses = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $ingresos = [];

        for ($i=0; $i < count($meses); $i++) {
            $month_start = date('Y/m/d', strtotime('first day of ' . $meses[$i] . ' this Year', time()));
            $month_end = date('Y/m/d', strtotime('last day of ' . $meses[$i] . ' this Year', time()));

            $ingresos[] = $this->getGananciasMes($month_start, $month_end);
        }


        return $ingresos;

    }
}
