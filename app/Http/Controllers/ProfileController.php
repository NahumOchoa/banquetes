<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
//    /**
//     * Función invocable del controlador del perfil
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function __invoke(){
//        return view('profile.edit');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('user_id', '=', auth()->id())->first();
        return view('configuracion.index', ['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //TODO: update profile information and validation
        $request->validate([
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'string|nullable|max:255',
            'phone_number' => 'nullable|max:10|min:10',
            'url_image' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'gender' => 'string|nullable|max:255',
        ]);
        $profile = Profile::where('user_id', '=', auth()->id())->first();

        if ($request->hasFile('url_image')) {
            $file = $request->file('url_image');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_image' => $filename]);
        }
        $file_path = public_path() . '/' . $profile->url_image;
        $profile->update($request->all());
        if($request->url_image != null){ // There is an image to update
            if($profile->url_image != null ){ // There is an image in the prject to delete
                if(file_exists($file_path)){
                    unlink($file_path);
                }
            }
            $profile->url_image = 'images/' . $name;
        }
        $profile->save();
        Alert::success('Éxito', 'Perfil Editado');
        return redirect()->route('profile.index');
    }

    /**
     * Show the form for editing the specified resource password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        return view('profile.password');
    }

}
