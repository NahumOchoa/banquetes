<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Package;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('packages.index', ['packages' => Package::with('menu')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.create', ['menus' => Menu::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|max:255',
            'quantity' => 'required|numeric|gt:9',
            'price' => ['required ', 'regex:/^(?:[1-9]\d+|\d)(?:\.\d?\d)?$/m'],
            'menu_id' => 'required|numeric'
        ]);
        //TODO: Better menu_id validation
        Package::create($request->all());
        Alert::success('Éxito', 'Paquete Creado');
        return redirect()->route('packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package  = Package::with('menu')->findOrFail($id);
        return view('packages.show', ['package' => $package]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package  = Package::findOrFail($id);
        return view('packages.edit', ['package' => $package, 'menus' => Menu::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package  = Package::findOrFail($id);
        $request->validate([
            'description' => 'required|max:255',
            'quantity' => 'required|numeric|gt:9',
            'price' => ['required ', 'regex:/^(?:[1-9]\d+|\d)(?:\.\d?\d)?$/m'],
            'menu_id' => 'required|numeric'
        ]);
        $package->update($request->all());
        Alert::success('Éxito', 'Paquete Editado');
        return redirect()->route('packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package  = Package::findOrFail($id);
        $package->delete();
        Alert::success('Éxito', 'Paquete Eliminado');
        return redirect()->route('packages.index')->with('success', 'Paquete Eliminado');
    }
}
