<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inventario.index', ['inventory' => Inventory::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventario.crear', ['inventory' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'quantity' => 'required|numeric',
            'description' => 'required|max:255',
            'url_image' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);
        if ($request->hasFile('url_image')) {
            $file = $request->file('url_image');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_image' => $filename]);
        }
        $inventory = Inventory::create($request->all());
        if($request->url_image != null){
            $inventory->url_image = 'images/' . $name;
        }
        $inventory->save();

        Alert::success('Éxito', 'Producto creado con éxito');
        return redirect()->route('inventario.index');
    }

    /**
     * Display the specified resource.
     * TODO: Is it possible to use the same add view with locked fields?
     * or use a pop up instead of a new view?
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('inventario.ver', [ 
            'inventory' => Inventory::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('inventario.crear', [
            'inventory' => Inventory::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'quantity' => 'required|numeric',
            'description' => 'required|max:255',
            'url_image' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);
        // TODO: If validate fail reurn Alert
        if ($request->hasFile('url_image')) {
            $file = $request->file('url_image');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_image' => $filename]);
        }
        $inventory = Inventory::findOrFail($id);
        $file_path = public_path() . '/' . $inventory->url_image;
        $inventory->update($request->all());
        if($request->url_image != null){ // There is an image to update
            if($inventory->url_image != null ){ // There is an image in the prject to delete
                if(file_exists($file_path)){
                    unlink($file_path);
                }
            }
            $inventory->url_image = 'images/' . $name;
        }
        $inventory->save();

        Alert::success('Éxito', 'Producto modificado con éxito');
        return redirect()->route('inventario.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inventory = Inventory::findOrFail($id);
        // Delete image from storage
        if($inventory->url_image != null){
            $image_path = public_path() . '/' . $inventory->url_image;
            if (file_exists($image_path)) {
                unlink($image_path);
            }
        }
        $inventory->delete();
        return redirect()->route('inventario.index');
    }
}
