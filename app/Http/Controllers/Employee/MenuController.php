<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu.index', ['menus' => Menu::with('category')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create', ['categories' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'price' => 'required|max:10000',
            'url_img' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'category_id' => 'required|numeric'
        ]);
        //TODO: More validation on category_id
        if ($request->hasFile('url_img')) {
            $file = $request->file('url_img');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_img' => $filename]);
        }
        $menu = Menu::create($request->all());
        if($request->url_img != null){
            $menu->url_img = 'images/' . $name;
        }
        $menu->save();
        Alert::success('Exito','Platillo Agregado');
        return redirect()->route('menus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::with('category')->findOrFail($id);
        return view('menu.show', ['menu' => $menu]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        $categories = Category::all();
        return view('menu.edit', [
            'menu' => $menu,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'price' => ['required ', 'regex:/^(?:[1-9]\d+|\d)(?:\.\d?\d)?$/m'],
            'url_img' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'category_id' => 'required|numeric'
        ]);
        if ($request->hasFile('url_img')) {
            $file = $request->file('url_img');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_img' => $filename]);
        }
        $file_path = public_path() . '/' . $menu->url_img;
        $menu->update($request->all());
        if($request->url_img != null){ // There is an image to update
            if($menu->url_img != null ){ // There is an image in the prject to delete
                if(file_exists($file_path)){
                    unlink($file_path);
                }
            }
            $menu->url_img = 'images/' . $name;
        }
        $menu->save();
        Alert::success('Exito','Platillo Actualizado');
        return redirect()->route('menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);
        // Delete image from storage
        if($menu->url_img != null){
            $image_path = public_path() . '\\' . $menu->url_img;
            if (file_exists($image_path)) {
                if (is_writable($image_path)){
                    unlink($image_path);
//                    dd($image_path);
                }
            }
        }
        if($menu->bookings()->exists()){
            Alert::error('Error','No es posible borrar este menu ya que existe en una reservación');
            return redirect()->route('menus.index');
        }
        $menu->delete();
        Alert::success('Exito','Platillo Eliminado');
        return redirect()->route('menus.index');
    }
}
