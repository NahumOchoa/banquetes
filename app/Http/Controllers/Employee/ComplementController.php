<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Complement;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ComplementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complements = Complement::all();
        return view('complements.index', ['complements' => $complements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('complements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'url_image' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);
        if ($request->hasFile('url_image')) {
            $file = $request->file('url_image');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_image' => $filename]);
        }
        $complement = Complement::create($request->all());
        if($request->url_image != null){
            $complement->url_image = 'images/' . $name;
        }
        $complement->save();
        Alert::success('Éxito', 'Complemento creado con éxito');
        return redirect()->route('complements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $complement = Complement::findOrFail($id);
        return view('complements.show', ['complement' => $complement]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $complement = Complement::findOrFail($id);
        return view('complements.edit', ['complement' => $complement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $complement = Complement::findOrFail($id);
        $request->validate([
            'name' => 'required|max:255',
            'url_image' => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);
        if ($request->hasFile('url_image')) {
            $file = $request->file('url_image');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $name);
            $filename = $file->getClientOriginalName();
            $request->merge(['url_image' => $filename]);
        }
        $file_path = public_path() . '/' . $complement->url_image;
        $complement->update($request->all());
        if($request->url_image != null){ // There is an image to update
            if($complement->url_image != null ){ // There is an image in the prject to delete
                if(file_exists($file_path)){
                    unlink($file_path);
                }
            }
            $complement->url_image = 'images/' . $name;
        }
        $complement->save();
        Alert::success('Éxito', 'Complemento modificado con éxito');
        return redirect()->route('complements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $complement = Complement::findOrFail($id);
        if($complement->url_image != null){
            $image_path = public_path() . '/' . $complement->url_image;
            if (file_exists($image_path)) {
                unlink($image_path);
            }
        }

        if($complement->complement_one()->exists() || $complement->complement_two()->exists()){
            Alert::error('Error','No es posible eliminar este complemento');
            return redirect()->route('complements.index');
        }

        $complement->delete();

        Alert::success('Éxito', 'Complemento Eliminado');
        return redirect()->route('complements.index');
    }
}
