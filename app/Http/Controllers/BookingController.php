<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Address;
use App\Models\BookingDetail;
use App\Models\Category;
use App\Models\Complement;
use App\Models\Role;
use App\Models\Menu;
use App\Models\Package;
use App\Models\Profile;
use App\Models\Service;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade\Pdf;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = BookingDetail::all();
        $statuses = Status::all();
        return view('reservaciones.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rol = Role::find(2);
        $clientes = $rol->users;

        $platillos = Menu::all();
        $paquetes = Package::all();

        $categoriasMenu = Category::all();
        $complementos = Complement::all();
        $servicios = Service::all();

        
        if (Gate::allows('is-admin') or Gate::allows('is-employee')){
            return view('reservaciones.crear', get_defined_vars());
        }
        if (Gate::allows('is-customer') or is_null(auth()->user())){
            return view('website.reservar', get_defined_vars());
        }
        // return view('reservaciones.crear', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'name'                  =>  'required|max:255',
            'booking_date'          =>  'required',
            'booking_time'          =>  'required',
            // 'user_id'               =>  'required',
            'description'           =>  'max:255',
            'zip_code'              =>  'required|numeric',
            'street_name'           =>  'required|max:255',
            'description_address'   =>  'max:255',
            // 'platillo'              =>  'required',
            'quantity'              =>  'required|numeric',
        ]);

        if(is_null(auth()->user())){
            Alert::warning('Oops', 'Solo usuarios registrados pueden realizar una reservación');
            return redirect()->route('website.reservar');
        }

        $subtotal = 0;
        if(isset($request['paquete'])){
            $subtotal = Package::select(['price'])->where('id', $request['paquete'])->first()->price;
        }
        elseif (isset($request['platillo'])) {
            $subtotal = Menu::select(['price'])->where('id', $request['platillo'])->first()->price * $request['quantity'];
        }

        
        // Get the id of the customer
        if (Gate::allows('is-customer')){
            $user_id = auth()->user()->id;
        }else{
            $user_id = $request['user_id'];
        }
        

        $booking = new Booking;
        isset($request['platillo'])         ? $booking->menu_id         = $request['platillo']          : '';
        isset($request['complementos'][0])  ? $booking->complement1_id  = $request['complementos'][0]   : '';
        isset($request['complementos'][1])  ? $booking->complement2_id  = $request['complementos'][1]   : '';
        isset($request['paquete'])          ? $booking->package_id      = $request['paquete']            : '';
        $booking->save();


        $address = Address::create([
            'street_name'   =>  $request['street_name'],
            'zip_code'      =>  $request['zip_code'],
            'description'   =>  $request['description_address'],
            'city_id'       =>  1,
        ]);


        $booking_details = BookingDetail::create([
            'name'          =>  $request['name'],
            'quantity'      =>  $request['quantity'],
            'description'   =>  $request['description'],
            'booking_date'  =>  $request['booking_date'],
            'booking_time'  =>  $request['booking_time'],
            // 'user_id'       =>  $request['user_id'],
            'user_id'       =>  $user_id,
            'address_id'    =>  $address->id,
            // 'subtotal'      =>  $request['quantity'] * $precioPlatillo,
            'subtotal'      =>  $subtotal,
            'total'         =>  $this->moneyToNumber($request['total']),
            'status_id'     =>  1,
            'booking_id'    =>  $booking->id,
        ]);

        if(isset($request['servicios'])){
            for ($i=0; $i < count($request['servicios']); $i++) { 
                $booking->services()->attach($request['servicios'][$i],['service_id'=>$request['servicios'][$i], 
                                                        'quantity'=>$request['quantityServices'][$i]]);
            }
        }

        $this->generateContract($booking_details, $booking);
        $this->generateInvoice($booking_details, $booking);



        if (Gate::allows('is-customer')){
            Alert::success('Éxito', 'Reservación creada con éxito');
            return redirect()->route('dashboardCustomer');
            
            // return view('dashboard.dashCliente', get_defined_vars());
        }
        Alert::success('Éxito', 'Reservación creada con éxito');
        return redirect()->route('reservaciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = BookingDetail::findOrFail($id);
        $services = Booking::find($id)->services;
        // $services = $booking->booking->services;

        $complement1 = Complement::find($booking->booking->complement1_id);
        $complement2 = Complement::find($booking->booking->complement2_id);

        $complements = [
            $complement1,
            $complement2,
        ];

        return view('reservaciones.ver', get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = BookingDetail::findOrFail($id);
        $platillos = Menu::all();
        $categoriasMenu = Category::all();
        $paquetes = Package::all();
        $servicios = Service::all();
        $serviciosActuales = Booking::find($booking->booking_id)->services;
        
        $rol = Role::find(2);
        $clientes = $rol->users;

        $complementos = Complement::all();
        $complement1 = Complement::find($booking->booking->complement1_id);
        $complement2 = Complement::find($booking->booking->complement2_id);

        $complementosEnReservacion = [
            $complement1,
            $complement2,
        ];
        return view('reservaciones.editar', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'                  =>  'required|max:255',
            'booking_date'          =>  'required',
            'booking_time'          =>  'required',
            'user_id'               =>  'required',
            'description'           =>  'max:255',
            'zip_code'              =>  'required|numeric',
            'street_name'           =>  'required|max:255',
            'description_address'   =>  'max:255',
            // 'platillo'              =>  'required',
            'quantity'              =>  'required|numeric',
        ]);

        $booking_details = BookingDetail::findOrFail($id);
        $booking = Booking::findOrFail($booking_details->booking_id);
        $address = Address::findOrFail($booking_details->address_id);
        $precioPlatillo = Menu::select(['price'])->where('id', $request['platillo'])->first()->price;


        $subtotal = 0;
        if(isset($request['paquete'])){
            $subtotal = Package::select(['price'])->where('id', $request['paquete'])->first()->price;
        }
        elseif (isset($request['platillo'])) {
            $subtotal = Menu::select(['price'])->where('id', $request['platillo'])->first()->price * $request['quantity'];
        }
        


        
        $booking->menu_id = $request['platillo'];
        isset($request['platillo'])         ? $booking->menu_id         = $request['platillo']          : '';
        isset($request['complementos'][0])  ? $booking->complement1_id  = $request['complementos'][0]   : '';
        isset($request['complementos'][1])  ? $booking->complement2_id  = $request['complementos'][1]   : '';
        isset($request['paquete'])          ? $booking->package_id      = $request['paquete']            : '';
        $booking->save();


        $address->update([
            'street_name'   =>  $request['street_name'],
            'zip_code'      =>  $request['zip_code'],
            'description'   =>  $request['description_address'],
            'city_id'       =>  1,
        ]);


        $booking_details->update([
            'name'          =>  $request['name'],
            'quantity'      =>  $request['quantity'],
            'description'   =>  $request['description'],
            'booking_date'  =>  $request['booking_date'],
            'booking_time'  =>  $request['booking_time'],
            'user_id'       =>  $request['user_id'],
            'address_id'    =>  $address->id,
            // 'subtotal'      =>  $request['quantity'] * $precioPlatillo,
            'subtotal'      =>  $subtotal,
            'total'         =>  $this->moneyToNumber($request['total']),
            'status_id'     =>  1,
            'booking_id'    =>  $booking->id,
        ]);
        
        $servicios = $booking->services;
        
        if(isset($request['servicios'])){
            for ($i=0; $i < count($request['servicios']); $i++) { 
                $contador = 0;
                foreach ($servicios as $servicio) {
                    if($servicio->pivot->service_id == $request['servicios'][$i]){
                        $servicio->pivot->update(['quantity'=>$request['quantityServices'][$i]]);
                        unset($servicios[$contador]);
                    }
                    $contador += 1;
                }
            }
        }

        foreach ($servicios as $servicio) {
            $servicio->pivot->delete();
        }

        

        $this->generateContract($booking_details, $booking);
        $this->generateInvoice($booking_details, $booking);

        Alert::success('Éxito', 'Reservación actualizada con éxito');
        return redirect()->route('reservaciones.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking_details = BookingDetail::findOrFail($id);
        $booking = Booking::findOrFail($booking_details->booking_id);
        $address = Address::findOrFail($booking_details->address_id);

        // Delete invoice from storage
        $invoice_path = public_path().'/invoices//'.'FACT-'.strval($booking_details->id).strval($booking_details->user->id).'.pdf';
        if (file_exists($invoice_path)) {
            unlink($invoice_path);
        }

        // Delete contract from storage
        $contract_path = public_path().'/contracts//'.'CONTRACT-'.strval($booking_details->id).strval($booking_details->user->id).'.pdf';
        if (file_exists($contract_path)) {
            unlink($contract_path);
        }

        $booking_details->delete();
        $booking->delete();
        $address->delete();
        
        return redirect()->route('reservaciones.index');
    }

    /**
     * Update the status of the resource
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updateStatus(Request $request){
        $bookingDetail = BookingDetail::findOrFail($request->id);
        $bookingDetail->status_id = $request->status;
        $bookingDetail->save();

        $data = ['mensaje' => 'Estado actualizado'];
        return response()->json($data);
    }


    /**
     * Convert a string with currency format to float value
     * 
     * @param string $vaule
     * @return double $valueWithoutComas
     */
    public function moneyToNumber($value) {
		$valueWithoutSignDollar = explode("$", $value);
		$valueWhitoutComas = str_replace(",", "", $valueWithoutSignDollar[1]);

		return (float)$valueWhitoutComas;
	}



    /** 
     * Genereate invoice of the booking
     * 
     * @param App\Models\BookingDetail $reservationDetails
     * @return int 0
    */
    private function generateInvoice($reservationDetails, $booking){
        $complement1 = Complement::find($reservationDetails->booking->complement1_id);
        $complement2 = Complement::find($reservationDetails->booking->complement2_id);

        $complementosEnReservacion = [
            $complement1,
            $complement2,
        ];

        $data = [
            'booking'       => $reservationDetails,
            'services'      => $this->addTotalPriceToService($booking->services),
            'complements'   => $complementosEnReservacion,
            'total'         => $this->numberToMoney($reservationDetails->total),
            'profile'       => Profile::where('user_id', '=', $reservationDetails->user->id)->first(),
        ];


        $filename = 'FACT-'.strval($reservationDetails->id).strval($reservationDetails->user->id);

        $pdf = PDF::loadView('facturas.plantillaFactura', $data);
        $pdf->save(public_path().'/invoices//'.$filename.'.pdf');

        return 0;
    }


    /** 
     * Genereate contract of the booking
     * 
     * @param App\Models\BookingDetail $reservationDetails
     * @return int 0
    */
    private function generateContract($reservationDetails, $booking){

        $data = [
            'booking'   => $reservationDetails,
            'services'  => $booking->services,
            'profile'   => Profile::where('user_id', '=', $reservationDetails->user->id)->first(),
        ];


        // $isUnhealthy = $booking->services->contains(function ($service, $i) {
        //     if ($service['name'] == 'Mesero') {
        //         print('id'. $i);
        //         return $service->pivot->quantity;
        //     }
        // });

        // dd($isUnhealthy);
        

        // dd(Arr::has($booking->services, 'service.name.Mesero'));
        // dd(Arr::has($booking->services, 'service.name.Mesero'));
        // dd($booking->services[1]->pivot->quantity);
        

        $filename = 'CONTRACT-'.strval($reservationDetails->id).strval($reservationDetails->user->id);

        $pdf = PDF::loadView('reservaciones.plantillaContrato', $data);
        $pdf->save(public_path().'/contracts//'.$filename.'.pdf');

        return 0;
    }


    /**
     * adds the total price in currency format
     * @param array $services
     * @return array
     */
    private function addTotalPriceToService($services){
        
        foreach ($services as $service) {
            $service = Arr::add($service, 'total', $this->numberToMoney($service->price * $service->pivot->quantity));
        }

        return $services;
    }


    /**
     * Convert double to string
     *
     * @param  double  $value
     * @return string
     */
    private function numberToMoney($value){
        return '$' . number_format($value, 2);
    }
}
