<?php

namespace App\Http\Controllers\Website;

use App\Models\Menu;
use App\Models\Category;
use App\Models\Complement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Return the index of the dishes
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $platillos = Menu::all();
        $categoriasMenu = Category::all();
        $complementos = Complement::all();
        return view('website.menu', get_defined_vars());
    }

    /**
     * Return the view with the search by the specified word
     * 
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search_name = $request->input('search_text');
        error_log($search_name);
        $platillos = Menu::where('name','like','%'.$search_name.'%')->orWhere('description','LIKE','%'.$search_name.'%')->get();
        $categoriasMenu = Category::all();
        $complementos = Complement::all();
        return view('website.menu', get_defined_vars());
    }

    /**
     * Return the view with the search by category
     * 
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchByCategory(Request $request)
    {
        $category_id = $request->category_id;
        $platillos = Menu::where('category_id','=',$category_id)->get();
        $categoriasMenu = Category::all();
        $complementos = Complement::all();
        return view('website.menu', get_defined_vars());
    }

}
